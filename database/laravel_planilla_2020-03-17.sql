-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         10.4.10-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.3.0.5877
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para laravel_planilla
CREATE DATABASE IF NOT EXISTS `laravel_planilla` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `laravel_planilla`;

-- Volcando estructura para tabla laravel_planilla.respuestas
CREATE TABLE IF NOT EXISTS `respuestas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `test` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `resp1` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp2` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp3` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp4` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp5` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp6` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp7` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp8` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp9` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp10` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp11` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp12` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp13` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp14` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp15` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp16` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp17` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp18` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp19` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp20` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp21` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp22` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp23` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp24` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp25` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp26` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp27` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp28` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp29` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp30` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp31` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp32` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp33` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp34` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp35` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp36` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp37` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp38` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp39` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp40` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp41` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp42` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp43` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp44` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp45` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp46` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp47` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp48` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp49` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp50` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp51` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp52` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp53` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp54` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp55` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp56` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp57` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp58` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp59` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp60` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp61` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp62` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp63` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp64` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp65` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp66` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp67` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp68` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp69` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp70` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp71` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp72` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp73` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp74` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp75` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp76` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp77` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp78` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp79` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp80` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp81` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp82` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp83` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp84` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp85` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp86` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp87` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp88` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp89` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp90` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp91` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp92` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp93` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp94` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp95` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp96` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp97` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp98` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp99` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resp100` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `respuestas_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla laravel_planilla.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `estado` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `sexo` char(2) COLLATE utf8_unicode_ci DEFAULT '',
  `nacimiento` date DEFAULT NULL,
  `escolaridad` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `puesto` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- La exportación de datos fue deseleccionada.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
