ALTER TABLE `usuarios`
	ADD COLUMN `estado` VARCHAR(15) NOT NULL DEFAULT '' AFTER `edad`,
	ADD COLUMN `sexo` CHAR(2) NOT NULL DEFAULT '' AFTER `estado`,
	ADD COLUMN `nacimiento` DATE NOT NULL AFTER `sexo`,
	ADD COLUMN `escolaridad` VARCHAR(50) NOT NULL DEFAULT '' AFTER `nacimiento`,
	ADD COLUMN `puesto` VARCHAR(50) NOT NULL DEFAULT '' AFTER `escolaridad`;

ALTER TABLE `usuarios`
	CHANGE COLUMN `nombre` `nombre` VARCHAR(255) NULL COLLATE 'utf8_unicode_ci' AFTER `id`,
	CHANGE COLUMN `edad` `edad` INT(11) NULL AFTER `nombre`,
	CHANGE COLUMN `estado` `estado` VARCHAR(15) NULL DEFAULT '' COLLATE 'utf8_unicode_ci' AFTER `edad`,
	CHANGE COLUMN `sexo` `sexo` CHAR(2) NULL DEFAULT '' COLLATE 'utf8_unicode_ci' AFTER `estado`,
	CHANGE COLUMN `nacimiento` `nacimiento` DATE NULL AFTER `sexo`,
	CHANGE COLUMN `escolaridad` `escolaridad` VARCHAR(50) NULL DEFAULT '' COLLATE 'utf8_unicode_ci' AFTER `nacimiento`,
	CHANGE COLUMN `puesto` `puesto` VARCHAR(50) NULL DEFAULT '' COLLATE 'utf8_unicode_ci' AFTER `escolaridad`;