$(function(){
	for (var i = 0; i < 90; i++) {
		$('input[name=resp'+i+']').attr('checked',false);
	}
	$("#parte_0").show();
	$("#parte_1,#parte_2,#parte_3,#parte_4,#parte_5,#parte_6,#parte_7,#parte_8,#parte_9,#parte_10").hide();

	$("#nombre,#edad").blur(function(){
		if($("#nombre").val() || $("#edad").val()){
			$("#btn_0").removeAttr('disabled');
		}
	});

    $("#btn_0").click(function(){
        $("#parte_1").show(500);
        $("#parte_0,#parte_2,#parte_3,#parte_4,#parte_5,#parte_6,#parte_7,#parte_8,#parte_9,#parte_10").hide(500);
    });
    $("#btn_1").click(function(){
        $("#parte_2").show(500);
        $("#parte_0,#parte_1,#parte_3,#parte_4,#parte_5,#parte_6,#parte_7,#parte_8,#parte_9,#parte_10").hide(500);
    });
    $("#btn_2").click(function(){
        $("#parte_3").show(500);
        $("#parte_0,#parte_1,#parte_2,#parte_4,#parte_5,#parte_6,#parte_7,#parte_8,#parte_9,#parte_10").hide(500);
    });
    $("#btn_3").click(function(){
        $("#parte_4").show(500);
        $("#parte_0,#parte_1,#parte_2,#parte_3,#parte_5,#parte_6,#parte_7,#parte_8,#parte_9,#parte_10").hide(500);
    });
    $("#btn_4").click(function(){
        $("#parte_5").show(500);
        $("#parte_0,#parte_1,#parte_2,#parte_3,#parte_4,#parte_6,#parte_7,#parte_8,#parte_9,#parte_10").hide(500);
    });
    $("#btn_5").click(function(){
        $("#parte_6").show(500);
        $("#parte_0,#parte_1,#parte_2,#parte_3,#parte_4,#parte_5,#parte_7,#parte_8,#parte_9,#parte_10").hide(500);
    });
    $("#btn_6").click(function(){
        $("#parte_7").show(500);
        $("#parte_0,#parte_1,#parte_2,#parte_3,#parte_4,#parte_5,#parte_6,#parte_8,#parte_9,#parte_10").hide(500);
    });
    $("#btn_7").click(function(){
        $("#parte_8").show(500);
        $("#parte_0,#parte_1,#parte_2,#parte_3,#parte_4,#parte_5,#parte_6,#parte_7,#parte_9,#parte_10").hide(500);
    });
    $("#btn_8").click(function(){
        $("#parte_9").show(500);
        $("#parte_0,#parte_1,#parte_2,#parte_3,#parte_4,#parte_5,#parte_6,#parte_7,#parte_8,#parte_10").hide(500);
    });
    $("#btn_9").click(function(){
        $("#parte_10").show(500);
        $("#parte_0,#parte_1,#parte_2,#parte_3,#parte_4,#parte_5,#parte_6,#parte_7,#parte_8,#parte_9").hide(500);
    });

     $("#formRegistro").submit(function(e) {
        e.preventDefault();
        var object = validateData();
        if(object !== 0){
            if (object.id) {
                update(object);
            }else{
                store(object);
            }
        }
    });
});

function validateData(){
    if($.trim($('#nombre').val()) === '' || $.trim($('#edad').val()) === ''){
        swal("¡OPERACIÓN FALLIDA!", "La información ingresada está incompleta, existen campos vacíos.", "warning");
        return 0;
    }else{
        return {
            'nombre':$('#nombre').val(),
            'edad':$('#edad').val(),
            'test':'IPV',
            'resp1':$('input:radio[name=resp1]:checked').val(),
			'resp2':$('input:radio[name=resp2]:checked').val(),
			'resp3':$('input:radio[name=resp3]:checked').val(),
			'resp4':$('input:radio[name=resp4]:checked').val(),
			'resp5':$('input:radio[name=resp5]:checked').val(),
			'resp6':$('input:radio[name=resp6]:checked').val(),
			'resp7':$('input:radio[name=resp7]:checked').val(),
			'resp8':$('input:radio[name=resp8]:checked').val(),
			'resp9':$('input:radio[name=resp9]:checked').val(),
			'resp10':$('input:radio[name=resp10]:checked').val(),
			'resp11':$('input:radio[name=resp11]:checked').val(),
			'resp12':$('input:radio[name=resp12]:checked').val(),
			'resp13':$('input:radio[name=resp13]:checked').val(),
			'resp14':$('input:radio[name=resp14]:checked').val(),
			'resp15':$('input:radio[name=resp15]:checked').val(),
			'resp16':$('input:radio[name=resp16]:checked').val(),
			'resp17':$('input:radio[name=resp17]:checked').val(),
			'resp18':$('input:radio[name=resp18]:checked').val(),
			'resp19':$('input:radio[name=resp19]:checked').val(),
			'resp20':$('input:radio[name=resp20]:checked').val(),
			'resp21':$('input:radio[name=resp21]:checked').val(),
			'resp22':$('input:radio[name=resp22]:checked').val(),
			'resp23':$('input:radio[name=resp23]:checked').val(),
			'resp24':$('input:radio[name=resp24]:checked').val(),
			'resp25':$('input:radio[name=resp25]:checked').val(),
			'resp26':$('input:radio[name=resp26]:checked').val(),
			'resp27':$('input:radio[name=resp27]:checked').val(),
			'resp28':$('input:radio[name=resp28]:checked').val(),
			'resp29':$('input:radio[name=resp29]:checked').val(),
			'resp30':$('input:radio[name=resp30]:checked').val(),
			'resp31':$('input:radio[name=resp31]:checked').val(),
			'resp32':$('input:radio[name=resp32]:checked').val(),
			'resp33':$('input:radio[name=resp33]:checked').val(),
			'resp34':$('input:radio[name=resp34]:checked').val(),
			'resp35':$('input:radio[name=resp35]:checked').val(),
			'resp36':$('input:radio[name=resp36]:checked').val(),
			'resp37':$('input:radio[name=resp37]:checked').val(),
			'resp38':$('input:radio[name=resp38]:checked').val(),
			'resp39':$('input:radio[name=resp39]:checked').val(),
			'resp40':$('input:radio[name=resp40]:checked').val(),
			'resp41':$('input:radio[name=resp41]:checked').val(),
			'resp42':$('input:radio[name=resp42]:checked').val(),
			'resp43':$('input:radio[name=resp43]:checked').val(),
			'resp44':$('input:radio[name=resp44]:checked').val(),
			'resp45':$('input:radio[name=resp45]:checked').val(),
			'resp46':$('input:radio[name=resp46]:checked').val(),
			'resp47':$('input:radio[name=resp47]:checked').val(),
			'resp48':$('input:radio[name=resp48]:checked').val(),
			'resp49':$('input:radio[name=resp49]:checked').val(),
			'resp50':$('input:radio[name=resp50]:checked').val(),
			'resp51':$('input:radio[name=resp51]:checked').val(),
			'resp52':$('input:radio[name=resp52]:checked').val(),
			'resp53':$('input:radio[name=resp53]:checked').val(),
			'resp54':$('input:radio[name=resp54]:checked').val(),
			'resp55':$('input:radio[name=resp55]:checked').val(),
			'resp56':$('input:radio[name=resp56]:checked').val(),
			'resp57':$('input:radio[name=resp57]:checked').val(),
			'resp58':$('input:radio[name=resp58]:checked').val(),
			'resp59':$('input:radio[name=resp59]:checked').val(),
			'resp60':$('input:radio[name=resp60]:checked').val(),
			'resp61':$('input:radio[name=resp61]:checked').val(),
			'resp62':$('input:radio[name=resp62]:checked').val(),
			'resp63':$('input:radio[name=resp63]:checked').val(),
			'resp64':$('input:radio[name=resp64]:checked').val(),
			'resp65':$('input:radio[name=resp65]:checked').val(),
			'resp66':$('input:radio[name=resp66]:checked').val(),
			'resp67':$('input:radio[name=resp67]:checked').val(),
			'resp68':$('input:radio[name=resp68]:checked').val(),
			'resp69':$('input:radio[name=resp69]:checked').val(),
			'resp70':$('input:radio[name=resp70]:checked').val(),
			'resp71':$('input:radio[name=resp71]:checked').val(),
			'resp72':$('input:radio[name=resp72]:checked').val(),
			'resp73':$('input:radio[name=resp73]:checked').val(),
			'resp74':$('input:radio[name=resp74]:checked').val(),
			'resp75':$('input:radio[name=resp75]:checked').val(),
			'resp76':$('input:radio[name=resp76]:checked').val(),
			'resp77':$('input:radio[name=resp77]:checked').val(),
			'resp78':$('input:radio[name=resp78]:checked').val(),
			'resp79':$('input:radio[name=resp79]:checked').val(),
			'resp80':$('input:radio[name=resp80]:checked').val(),
			'resp81':$('input:radio[name=resp81]:checked').val(),
			'resp82':$('input:radio[name=resp82]:checked').val(),
			'resp83':$('input:radio[name=resp83]:checked').val(),
			'resp84':$('input:radio[name=resp84]:checked').val(),
			'resp85':$('input:radio[name=resp85]:checked').val(),
			'resp86':$('input:radio[name=resp86]:checked').val(),
			'resp87':$('input:radio[name=resp87]:checked').val(),
        };
    }
}

function store(object){
    $.ajax({
        url: 'ipv/store',
        type: 'POST',
        data: object,
        success:function(data){
            swal("¡OPERACIÓN EXITOSA!", "El registro ha sido almacenado", "success");
            $("#parte_0").show();
			$("#parte_1,#parte_2,#parte_3,#parte_4,#parte_5,#parte_6,#parte_7,#parte_8,#parte_9,#parte_10").hide();
			$('#nombre,#edad').val('');
			for (var i = 0; i < 90; i++) {
				$('input[name=resp'+i+']').attr('checked',false);
			}
			window.open('ipv/excel/'+data['id']);
        },
        error:function(data){
            swal("¡OPERACIÓN FALLIDA!", data.responseJSON['message'], "error");
        },
    });
}

function checkRespuesta(seccion){
	switch(seccion) {
		case 1:
			if ($('input:radio[name=resp1]:checked').val()) {
				if ($('input:radio[name=resp2]:checked').val()) {
					if ($('input:radio[name=resp3]:checked').val()) {
						if ($('input:radio[name=resp4]:checked').val()) {
							if ($('input:radio[name=resp5]:checked').val()) {
								if ($('input:radio[name=resp6]:checked').val()) {
									if ($('input:radio[name=resp7]:checked').val()) {
										if ($('input:radio[name=resp8]:checked').val()) {
											if ($('input:radio[name=resp9]:checked').val()) {
												if ($('input:radio[name=resp10]:checked').val()) {
													$("#btn_1").removeAttr('disabled');
												}
											}
										}
									}
								}
							}
						}
					}	
				}
			}
		break;
		case 2:
			if ($('input:radio[name=resp11]:checked').val()) {
				if ($('input:radio[name=resp12]:checked').val()) {
					if ($('input:radio[name=resp13]:checked').val()) {
						if ($('input:radio[name=resp14]:checked').val()) {
							if ($('input:radio[name=resp15]:checked').val()) {
								if ($('input:radio[name=resp16]:checked').val()) {
									if ($('input:radio[name=resp17]:checked').val()) {
										if ($('input:radio[name=resp18]:checked').val()) {
											if ($('input:radio[name=resp19]:checked').val()) {
												if ($('input:radio[name=resp20]:checked').val()) {
													$("#btn_2").removeAttr('disabled');
												}
											}
										}
									}
								}
							}
						}
					}	
				}
			}
		break;
		case 3:
			if ($('input:radio[name=resp21]:checked').val()) {
				if ($('input:radio[name=resp22]:checked').val()) {
					if ($('input:radio[name=resp23]:checked').val()) {
						if ($('input:radio[name=resp24]:checked').val()) {
							if ($('input:radio[name=resp25]:checked').val()) {
								if ($('input:radio[name=resp26]:checked').val()) {
									if ($('input:radio[name=resp27]:checked').val()) {
										if ($('input:radio[name=resp28]:checked').val()) {
											if ($('input:radio[name=resp29]:checked').val()) {
												if ($('input:radio[name=resp30]:checked').val()) {
													$("#btn_3").removeAttr('disabled');
												}
											}
										}
									}
								}
							}
						}
					}	
				}
			}
		break;
		case 4:
			if ($('input:radio[name=resp31]:checked').val()) {
				if ($('input:radio[name=resp32]:checked').val()) {
					if ($('input:radio[name=resp33]:checked').val()) {
						if ($('input:radio[name=resp34]:checked').val()) {
							if ($('input:radio[name=resp35]:checked').val()) {
								if ($('input:radio[name=resp36]:checked').val()) {
									if ($('input:radio[name=resp37]:checked').val()) {
										if ($('input:radio[name=resp38]:checked').val()) {
											if ($('input:radio[name=resp39]:checked').val()) {
												if ($('input:radio[name=resp40]:checked').val()) {
													$("#btn_4").removeAttr('disabled');
												}
											}
										}
									}
								}
							}
						}
					}	
				}
			}
		break;
		case 5:
			if ($('input:radio[name=resp41]:checked').val()) {
				if ($('input:radio[name=resp42]:checked').val()) {
					if ($('input:radio[name=resp43]:checked').val()) {
						if ($('input:radio[name=resp44]:checked').val()) {
							if ($('input:radio[name=resp45]:checked').val()) {
								if ($('input:radio[name=resp46]:checked').val()) {
									if ($('input:radio[name=resp47]:checked').val()) {
										if ($('input:radio[name=resp48]:checked').val()) {
											if ($('input:radio[name=resp49]:checked').val()) {
												if ($('input:radio[name=resp50]:checked').val()) {
													$("#btn_5").removeAttr('disabled');
												}
											}
										}
									}
								}
							}
						}
					}	
				}
			}
		break;
		case 6:
			if ($('input:radio[name=resp51]:checked').val()) {
				if ($('input:radio[name=resp52]:checked').val()) {
					if ($('input:radio[name=resp53]:checked').val()) {
						if ($('input:radio[name=resp55]:checked').val()) {
							if ($('input:radio[name=resp55]:checked').val()) {
								if ($('input:radio[name=resp56]:checked').val()) {
									if ($('input:radio[name=resp57]:checked').val()) {
										if ($('input:radio[name=resp58]:checked').val()) {
											if ($('input:radio[name=resp59]:checked').val()) {
												if ($('input:radio[name=resp60]:checked').val()) {
													$("#btn_6").removeAttr('disabled');
												}
											}
										}
									}
								}
							}
						}
					}	
				}
			}
		break;
		case 7:
			if ($('input:radio[name=resp61]:checked').val()) {
				if ($('input:radio[name=resp62]:checked').val()) {
					if ($('input:radio[name=resp63]:checked').val()) {
						if ($('input:radio[name=resp65]:checked').val()) {
							if ($('input:radio[name=resp65]:checked').val()) {
								if ($('input:radio[name=resp66]:checked').val()) {
									if ($('input:radio[name=resp67]:checked').val()) {
										if ($('input:radio[name=resp68]:checked').val()) {
											if ($('input:radio[name=resp69]:checked').val()) {
												if ($('input:radio[name=resp70]:checked').val()) {
													$("#btn_7").removeAttr('disabled');
												}
											}
										}
									}
								}
							}
						}
					}	
				}
			}
		break;
		case 8:
			if ($('input:radio[name=resp71]:checked').val()) {
				if ($('input:radio[name=resp72]:checked').val()) {
					if ($('input:radio[name=resp73]:checked').val()) {
						if ($('input:radio[name=resp75]:checked').val()) {
							if ($('input:radio[name=resp75]:checked').val()) {
								if ($('input:radio[name=resp76]:checked').val()) {
									if ($('input:radio[name=resp77]:checked').val()) {
										if ($('input:radio[name=resp78]:checked').val()) {
											if ($('input:radio[name=resp79]:checked').val()) {
												if ($('input:radio[name=resp80]:checked').val()) {
													$("#btn_8").removeAttr('disabled');
												}
											}
										}
									}
								}
							}
						}
					}	
				}
			}
		break;
		case 9:
			if ($('input:radio[name=resp81]:checked').val()) {
				if ($('input:radio[name=resp82]:checked').val()) {
					if ($('input:radio[name=resp83]:checked').val()) {
						if ($('input:radio[name=resp85]:checked').val()) {
							if ($('input:radio[name=resp85]:checked').val()) {
								if ($('input:radio[name=resp86]:checked').val()) {
									if ($('input:radio[name=resp87]:checked').val()) {
										$("#btn_9").removeAttr('disabled');
									}
								}
							}
						}
					}	
				}
			}
		break;
	}
}