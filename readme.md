# Pruebas Psicométricas | Amaxonia ERP

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local_

### Instalación 🔧

* Clonar el repo
* Ingresar a la raiz del proyecto y ejecutar en la consola el comando **composer install**
* Copiar el archivo **.env** a la raiz del proyecto
* Crear la base de datos y exportar SQL.
* Ejecutar en consola **php artisan key:generate**
* Verificar en el archivo **.env** los datos de configuración de la base de datos
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=nombre_de_tabla
DB_USERNAME=usuario_base_datos
DB_PASSWORD=password_base_datos
```