<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuarios;
use App\Respuestas;
use DB;
use Excel;

class UsuariosController extends Controller
{
    public function index()
    {
        return view('usuarios.create');
    }

    public function create()
    {
        return view('usuarios.create');
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
                $usuario = new Usuarios();

                $usuario->nombre = $request->get('nombre');        
                $usuario->edad   = $request->get('edad');        
                $usuario->save();

                $respuesta = new Respuestas();

                $respuesta->user_id = $usuario->id;
                $respuesta->test    = $request->get('test');
                $respuesta->resp1   = $request->get('resp1'); 
                $respuesta->resp2   = $request->get('resp2'); 
                $respuesta->resp3   = $request->get('resp3'); 
                $respuesta->resp4   = $request->get('resp4'); 
                $respuesta->resp5   = $request->get('resp5');
                $respuesta->resp6   = $request->get('resp6');
                $respuesta->resp7   = $request->get('resp7');
                $respuesta->resp8   = $request->get('resp8');
                $respuesta->resp9   = $request->get('resp9');
                $respuesta->resp10  = $request->get('resp10');
                $respuesta->resp11  = $request->get('resp11');
                $respuesta->resp12  = $request->get('resp12');
                $respuesta->resp13  = $request->get('resp13');
                $respuesta->resp14  = $request->get('resp14');
                $respuesta->resp15  = $request->get('resp15');
                $respuesta->resp16  = $request->get('resp16');
                $respuesta->resp17  = $request->get('resp17');
                $respuesta->resp18  = $request->get('resp18');
                $respuesta->resp19  = $request->get('resp19');
                $respuesta->resp20  = $request->get('resp20');
                $respuesta->resp21  = $request->get('resp21');
                $respuesta->resp22  = $request->get('resp22');
                $respuesta->resp23  = $request->get('resp23');
                $respuesta->resp24  = $request->get('resp24');
                $respuesta->resp25  = $request->get('resp25');
                $respuesta->resp26  = $request->get('resp26');
                $respuesta->resp27  = $request->get('resp27');
                $respuesta->resp28  = $request->get('resp28');
                $respuesta->resp29  = $request->get('resp29');
                $respuesta->resp30  = $request->get('resp30');
                $respuesta->resp31  = $request->get('resp31');
                $respuesta->resp32  = $request->get('resp32');
                $respuesta->resp33  = $request->get('resp33');
                $respuesta->resp34  = $request->get('resp34');
                $respuesta->resp35  = $request->get('resp35');
                $respuesta->resp36  = $request->get('resp36');
                $respuesta->resp37  = $request->get('resp37');
                $respuesta->resp38  = $request->get('resp38');
                $respuesta->resp39  = $request->get('resp39');
                $respuesta->resp40  = $request->get('resp40');
                $respuesta->resp41  = $request->get('resp41');
                $respuesta->resp42  = $request->get('resp42');
                $respuesta->resp43  = $request->get('resp43');
                $respuesta->resp44  = $request->get('resp44');
                $respuesta->resp45  = $request->get('resp45');
                $respuesta->resp46  = $request->get('resp46');
                $respuesta->resp47  = $request->get('resp47');
                $respuesta->resp48  = $request->get('resp48');
                $respuesta->resp49  = $request->get('resp49');
                $respuesta->resp50  = $request->get('resp50');
                $respuesta->resp51  = $request->get('resp51');
                $respuesta->resp52  = $request->get('resp52');
                $respuesta->resp53  = $request->get('resp53');
                $respuesta->resp54  = $request->get('resp54');
                $respuesta->resp55  = $request->get('resp55');
                $respuesta->resp56  = $request->get('resp56');
                $respuesta->resp57  = $request->get('resp57');
                $respuesta->resp58  = $request->get('resp58');
                $respuesta->resp59  = $request->get('resp59');
                $respuesta->resp60  = $request->get('resp60');
                $respuesta->resp61  = $request->get('resp61');
                $respuesta->resp62  = $request->get('resp62');
                $respuesta->resp63  = $request->get('resp63');
                $respuesta->resp64  = $request->get('resp64');
                $respuesta->resp65  = $request->get('resp65');
                $respuesta->resp66  = $request->get('resp66');
                $respuesta->resp67  = $request->get('resp67');
                $respuesta->resp68  = $request->get('resp68');
                $respuesta->resp69  = $request->get('resp69');
                $respuesta->resp70  = $request->get('resp70');
                $respuesta->resp71  = $request->get('resp71');
                $respuesta->resp72  = $request->get('resp72');
                $respuesta->resp73  = $request->get('resp73');
                $respuesta->resp74  = $request->get('resp74');
                $respuesta->resp75  = $request->get('resp75');
                $respuesta->resp76  = $request->get('resp76');
                $respuesta->resp77  = $request->get('resp77');
                $respuesta->resp78  = $request->get('resp78');
                $respuesta->resp79  = $request->get('resp79');
                $respuesta->resp80  = $request->get('resp80');
                $respuesta->resp81  = $request->get('resp81');
                $respuesta->resp82  = $request->get('resp82');
                $respuesta->resp83  = $request->get('resp83');
                $respuesta->resp84  = $request->get('resp84');
                $respuesta->resp85  = $request->get('resp85');
                $respuesta->resp86  = $request->get('resp86');
                $respuesta->resp87  = $request->get('resp87');
                $store = $respuesta->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        if($store){
            return response()->json(['status' => 201, 'id' => $usuario->id]);
        }
        else
            return response()->json('Error',500);
    }

    public function excel($id)
    {
        Excel::create('Resultado IPV', function($excel) use ($id) {
            $data = DB::table('respuestas')->join('usuarios', 'respuestas.user_id', '=', 'usuarios.id')
            ->select('usuarios.nombre','usuarios.edad','respuestas.*')
            ->where('respuestas.test', '=','IPV')
            ->where('respuestas.user_id', '=',$id)
            ->get();
            
            $data = json_decode( json_encode($data), true);

            $excel->setTitle('Resultado IPV');
            $excel->setCreator('Amaxonia ERP')->setCompany('Amaxonia ERP');

             $excel->sheet('Resultado IPV', function($sheet) use($data){

                $sheet->setFontFamily('Arial');

                $sheet->mergeCells("B2:C2");
                $sheet->getStyle('B2')->getFont()->setBold(true);
                $sheet->setCellValue("B2","Nombre:");
                $sheet->mergeCells("D2:G2");
                $sheet->setCellValue("D2",$data[0]['nombre']);

                $sheet->getStyle('H2')->getFont()->setBold(true);
                $sheet->setCellValue("H2","Edad:");
                $sheet->setCellValue("I2",$data[0]['edad']);

                $sheet->getStyle('B4:P4')->getFont()->setBold(true);
                $sheet->getStyle('R4:R22')->getFont()->setBold(true);
                $sheet->setCellValue("R4","DGV");
                $sheet->setCellValue("R6","I");
                $sheet->setCellValue("R8","II");
                $sheet->setCellValue("R10","III");
                $sheet->setCellValue("R12","IV");
                $sheet->setCellValue("R14","V");
                $sheet->setCellValue("R16","VI");
                $sheet->setCellValue("R20","VII");
                $sheet->setCellValue("R22","IX");

                $sheet->getStyle('B24:B27')->getFont()->setBold(true);
                $sheet->mergeCells("B24:C24");
                $sheet->setCellValue("B24","LEYENDA");
                $sheet->mergeCells("B25:C25");
                $sheet->setCellValue("B25","1 es A");
                $sheet->mergeCells("B26:C26");
                $sheet->setCellValue("B26","2 es B");
                $sheet->mergeCells("B27:C27");
                $sheet->setCellValue("B27","3 es C");

                $sheet->getColumnDimension('D')->setVisible(false);
                $sheet->getColumnDimension('G')->setVisible(false);
                $sheet->getColumnDimension('J')->setVisible(false);
                $sheet->getColumnDimension('M')->setVisible(false);
                $sheet->getColumnDimension('P')->setVisible(false);

                $sheet->setWidth(array(
                    'A'     =>  6,
                    'B'     =>  6,
                    'C'     =>  6,
                    'D'     =>  6,
                    'E'     =>  6,
                    'F'     =>  6,
                    'G'     =>  6,
                    'H'     =>  6,
                    'I'     =>  6,
                    'J'     =>  6,
                    'K'     =>  6,
                    'L'     =>  6,
                    'M'     =>  6,
                    'N'     =>  6,
                    'O'     =>  6,
                    'P'     =>  6,
                    'Q'     =>  6,
                    'R'     =>  6
                ));

                $sheet->cells('A1:AZ100', function ($cells) {
                    $cells->setBackground('#ffffff');
                });

                $sheet->cells('Q4', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('Q5:Q7', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('Q8:Q9', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('Q10', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('Q11:Q13', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('Q14', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('Q15:Q16', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('Q17:Q19', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('Q20:Q21', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('Q22', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('B5:O22', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('C5:C22', function ($cells) {
                    $cells->setFontColor('#ff0000');
                    $cells->setBackground('#ffff99');
                });

                $sheet->cells('F5:F22', function ($cells) {
                    $cells->setFontColor('#ff0000');
                    $cells->setBackground('#ffff99');
                });

                $sheet->cells('I5:I22', function ($cells) {
                    $cells->setFontColor('#ff0000');
                    $cells->setBackground('#ffff99');
                });

                $sheet->cells('L5:L22', function ($cells) {
                    $cells->setFontColor('#ff0000');
                    $cells->setBackground('#ffff99');
                });

                $sheet->cells('O5:O19', function ($cells) {
                    $cells->setFontColor('#ff0000');
                    $cells->setBackground('#ffff99');
                });

                $ln = 5;
                $r=0;
                $x=0;
                $rowArray = [];

                for ($i=0; $i < 18 ; $i++) {
                    $sheet->setCellValue("B".($ln+$i),$i+1);
                    $sheet->getStyle("B".($ln+$i))->getFont()->setBold(true);
                    $sheet->setCellValue("C".($ln+$i),$data[0]['resp'.($i+1)]);

                    if (($i+1)==1) {
                        if ($data[0]['resp'.($i+1)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==2) {
                        if ($data[0]['resp'.($i+1)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==3) {
                        if ($data[0]['resp'.($i+1)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==4) {
                        if ($data[0]['resp'.($i+1)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==5) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==6) {
                        if ($data[0]['resp'.($i+1)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==7) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==8) {
                        if ($data[0]['resp'.($i+1)] == 0) {
                            $r = 0;
                        }else{
                            $r = 1;
                        }
                    }else if (($i+1)==9) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==10) {
                        if ($data[0]['resp'.($i+1)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==11) {
                        if ($data[0]['resp'.($i+1)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==12) {
                        if ($data[0]['resp'.($i+1)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==13) {
                        if ($data[0]['resp'.($i+1)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==14) {
                        if ($data[0]['resp'.($i+1)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==15) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==16) {
                        if ($data[0]['resp'.($i+1)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==17) {
                        if ($data[0]['resp'.($i+1)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i+1)==18) {
                        if ($data[0]['resp'.($i+1)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }

                    $sheet->setCellValue("D".($ln+$i),$r);
                }
                $j = 0;
                for ($i=19; $i < 37 ; $i++) {
                    $sheet->setCellValue("E".($ln+$j),$i);
                    $sheet->getStyle("E".($ln+$j))->getFont()->setBold(true);
                    $sheet->setCellValue("F".($ln+$j),$data[0]['resp'.$i]);

                    if (($i)==19) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==20) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==21) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==22) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==23) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==24) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==25) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==26) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 0;
                        }else{
                            $r = 1;
                        }
                    }else if (($i)==27) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==28) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==29) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==30) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==31) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==32) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==33) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==34) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==35) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==36) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }
                    
                    $sheet->setCellValue("G".$ln,$r);
                    $j++;
                }

                $j = 0;
                for ($i=37; $i < 55 ; $i++) {
                    $sheet->setCellValue("H".($ln+$j),$i);
                    $sheet->getStyle("H".($ln+$j))->getFont()->setBold(true);
                    $sheet->setCellValue("I".($ln+$j),$data[0]['resp'.$i]);

                    if (($i)==37) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==38) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==39) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==40) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==41) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==42) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==43) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==44) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 0;
                        }else{
                            $r = 1;
                        }
                    }else if (($i)==45) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==46) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==47) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==48) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==49) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==50) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==51) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==52) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==53) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==54) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }
                    
                    $sheet->setCellValue("J".$ln,$r);
                    $j++;
                }

                $j = 0;
                for ($i=55; $i < 73 ; $i++) {
                    $sheet->setCellValue("K".($ln+$j),$i);
                    $sheet->getStyle("K".($ln+$j))->getFont()->setBold(true);
                    $sheet->setCellValue("L".($ln+$j),$data[0]['resp'.$i]);
                    
                    if (($i)==55) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==56) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==57) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==58) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==59) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==60) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==61) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==62) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 0;
                        }else{
                            $r = 1;
                        }
                    }else if (($i)==63) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==64) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==65) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==66) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==67) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==68) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==69) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==70) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==71) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==72) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }

                    $sheet->setCellValue("M".$ln,$r);
                    $j++;
                }

                $j = 0;
                for ($i=73; $i < 88 ; $i++) {
                    $sheet->setCellValue("N".($ln+$j),$i);
                    $sheet->getStyle("N".($ln+$j))->getFont()->setBold(true);
                    $sheet->setCellValue("O".($ln+$j),$data[0]['resp'.$i]);

                    if (($i)==73) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==74) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==75) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==76) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==77) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==78) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==79) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==80) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 0;
                        }else{
                            $r = 1;
                        }
                    }else if (($i)==81) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==82) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==83) {
                        if ($data[0]['resp'.($i)] == 2) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==84) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==85) {
                        if ($data[0]['resp'.($i)] == 3) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==86) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }else if (($i)==87) {
                        if ($data[0]['resp'.($i)] == 1) {
                            $r = 1;
                        }else{
                            $r = 0;
                        }
                    }

                    $sheet->setCellValue("P".$ln,$r);
                    $j++;
                }

                $sheet->getStyle("Q4:Q22")->getFont()->setBold(true);
                $sheet->setCellValue("Q4","=D5+D6+D7+D8+D9+D10+D11+D14+D15+D16+D17+D18+D19+D20+D21+D22+G13+G21+D13+P4");
                $sheet->setCellValue("Q6","=D5+D6+G5+G6+J5+J6+M5+M6+P5+P6+P7");
                $sheet->setCellValue("Q8","=D7+D8+G7+G8+J7+J8+M7+M8+M9+P8+P9");
                $sheet->setCellValue("Q10","=D9+D10+D11+G9+G10+G11+J9+J10+J11+M10+P10");
                $sheet->setCellValue("Q12","=8-(D12+G12+J12+M11+M12+P11+P12+P13)");
                $sheet->setCellValue("Q14","=D13+D14+D15+G13+G14+G15+J13+J14+M13+M14+P14");
                $sheet->setCellValue("Q16","=D16+D17+G16+G17+J15+J16+J17+M15+M16+P15+P16");
                $sheet->setCellValue("Q18","=D18+G18+J18+M17+M18+P17+P18+P19");
                $sheet->setCellValue("Q20","=D19+G19+G20+J19+J20+M19+M20+M21");
                $sheet->setCellValue("Q22","=D20+D21+D22+G21+G22+J21+J22+M22");
             });
        })->export('xlsx');
        return view('usuarios.create');
    }
}