<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuarios;
use App\Respuestas;
use DB;
use Excel;

class CleaverController extends Controller
{
    public function index()
    {
        return view('cleaver.create');
    }

    public function create()
    {
        return view('cleaver.create');
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
                $usuario = new Usuarios();

                $usuario->nombre      = strtoupper($request->get('nombre'));
                $usuario->estado      = strtoupper($request->get('estado'));
                $usuario->sexo        = strtoupper($request->get('sexo'));
                $usuario->nacimiento  = date("Y-m-d", strtotime($request->get('nacimiento')));
                $usuario->escolaridad = strtoupper($request->get('escolaridad'));
                $usuario->puesto      = strtoupper($request->get('puesto'));
                $store                = $usuario->save();

                $respuesta = new Respuestas();

                $respuesta->user_id = $usuario->id;
                $respuesta->test    = $request->get('test'); 
                $respuesta->resp1   = $request->get('resp1'); 
                $respuesta->resp2   = $request->get('resp2'); 
                $respuesta->resp3   = $request->get('resp3'); 
                $respuesta->resp4   = $request->get('resp4'); 
                $respuesta->resp5   = $request->get('resp5');
                $respuesta->resp6   = $request->get('resp6');
                $respuesta->resp7   = $request->get('resp7');
                $respuesta->resp8   = $request->get('resp8');
                $respuesta->resp9   = $request->get('resp9');
                $respuesta->resp10  = $request->get('resp10');
                $respuesta->resp11  = $request->get('resp11');
                $respuesta->resp12  = $request->get('resp12');
                $respuesta->resp13  = $request->get('resp13');
                $respuesta->resp14  = $request->get('resp14');
                $respuesta->resp15  = $request->get('resp15');
                $respuesta->resp16  = $request->get('resp16');
                $respuesta->resp17  = $request->get('resp17');
                $respuesta->resp18  = $request->get('resp18');
                $respuesta->resp19  = $request->get('resp19');
                $respuesta->resp20  = $request->get('resp20');
                $respuesta->resp21  = $request->get('resp21');
                $respuesta->resp22  = $request->get('resp22');
                $respuesta->resp23  = $request->get('resp23');
                $respuesta->resp24  = $request->get('resp24');
                $respuesta->resp25  = $request->get('resp25');
                $respuesta->resp26  = $request->get('resp26');
                $respuesta->resp27  = $request->get('resp27');
                $respuesta->resp28  = $request->get('resp28');
                $respuesta->resp29  = $request->get('resp29');
                $respuesta->resp30  = $request->get('resp30');
                $respuesta->resp31  = $request->get('resp31');
                $respuesta->resp32  = $request->get('resp32');
                $respuesta->resp33  = $request->get('resp33');
                $respuesta->resp34  = $request->get('resp34');
                $respuesta->resp35  = $request->get('resp35');
                $respuesta->resp36  = $request->get('resp36');
                $respuesta->resp37  = $request->get('resp37');
                $respuesta->resp38  = $request->get('resp38');
                $respuesta->resp39  = $request->get('resp39');
                $respuesta->resp40  = $request->get('resp40');
                $respuesta->resp41  = $request->get('resp41');
                $respuesta->resp42  = $request->get('resp42');
                $respuesta->resp43  = $request->get('resp43');
                $respuesta->resp44  = $request->get('resp44');
                $respuesta->resp45  = $request->get('resp45');
                $respuesta->resp46  = $request->get('resp46');
                $respuesta->resp47  = $request->get('resp47');
                $respuesta->resp48  = $request->get('resp48');
                $respuesta->resp49  = $request->get('resp49');
                $respuesta->resp50  = $request->get('resp50');
                $respuesta->resp51  = $request->get('resp51');
                $respuesta->resp52  = $request->get('resp52');
                $respuesta->resp53  = $request->get('resp53');
                $respuesta->resp54  = $request->get('resp54');
                $respuesta->resp55  = $request->get('resp55');
                $respuesta->resp56  = $request->get('resp56');
                $respuesta->resp57  = $request->get('resp57');
                $respuesta->resp58  = $request->get('resp58');
                $respuesta->resp59  = $request->get('resp59');
                $respuesta->resp60  = $request->get('resp60');
                $respuesta->resp61  = $request->get('resp61');
                $respuesta->resp62  = $request->get('resp62');
                $respuesta->resp63  = $request->get('resp63');
                $respuesta->resp64  = $request->get('resp64');
                $respuesta->resp65  = $request->get('resp65');
                $respuesta->resp66  = $request->get('resp66');
                $respuesta->resp67  = $request->get('resp67');
                $respuesta->resp68  = $request->get('resp68');
                $respuesta->resp69  = $request->get('resp69');
                $respuesta->resp70  = $request->get('resp70');
                $respuesta->resp71  = $request->get('resp71');
                $respuesta->resp72  = $request->get('resp72');
                $respuesta->resp73  = $request->get('resp73');
                $respuesta->resp74  = $request->get('resp74');
                $respuesta->resp75  = $request->get('resp75');
                $respuesta->resp76  = $request->get('resp76');
                $respuesta->resp77  = $request->get('resp77');
                $respuesta->resp78  = $request->get('resp78');
                $respuesta->resp79  = $request->get('resp79');
                $respuesta->resp80  = $request->get('resp80');
                $respuesta->resp81  = $request->get('resp81');
                $respuesta->resp82  = $request->get('resp82');
                $respuesta->resp83  = $request->get('resp83');
                $respuesta->resp84  = $request->get('resp84');
                $respuesta->resp85  = $request->get('resp85');
                $respuesta->resp86  = $request->get('resp86');
                $respuesta->resp87  = $request->get('resp87');
                $respuesta->resp88  = $request->get('resp88');
                $respuesta->resp89  = $request->get('resp89');
                $respuesta->resp90  = $request->get('resp90');
                $respuesta->resp91  = $request->get('resp91');
                $respuesta->resp92  = $request->get('resp92');
                $respuesta->resp93  = $request->get('resp93');
                $respuesta->resp94  = $request->get('resp94');
                $respuesta->resp95  = $request->get('resp95');
                $respuesta->resp96  = $request->get('resp96');
                $store = $respuesta->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }


        if($store){
            return response()->json(['status' => 201, 'id' => $usuario->id]);
        }
        else
            return response()->json('Error',500);
    }

    public function excel($id)
    {
        Excel::create('Resultado Cleaver', function($excel) use($id) {
            $data = DB::table('respuestas')->join('usuarios', 'respuestas.user_id', '=', 'usuarios.id')
            ->select('usuarios.*','respuestas.*')
            ->where('respuestas.user_id', '=',$id)
            ->where('respuestas.test', '=','CLEAVER')
            ->get();
            
            $data = json_decode( json_encode($data), true);

            list($ano,$mes,$dia) = explode("-",$data[0]['nacimiento']);
            $ano_diferencia = date("Y") - $ano;
            $mes_diferencia = date("m") - $mes;
            $dia_diferencia = date("d") - $dia;
            if ($dia_diferencia < 0 || $mes_diferencia < 0)
                $ano_diferencia--;

            $data[0]['edad'] = $ano_diferencia;

            $excel->setTitle('Resultado Cleaver');
            $excel->setCreator('Amaxonia ERP')->setCompany('Amaxonia ERP');

            $excel->sheet('Resultado Cleaver', function($sheet) use($data){
                $sheet->setFontFamily('Arial');

                $sheet->getStyle('B2')->getFont()->setBold(true);
                $sheet->setCellValue("B2","Nombre:");
                $sheet->mergeCells("C2:E2");
                $sheet->setCellValue("C2",$data[0]['nombre']);

                $sheet->getStyle('H2')->getFont()->setBold(true);
                $sheet->setCellValue("H2","Puesto:");
                $sheet->mergeCells("I2:K2");
                $sheet->setCellValue("I2",$data[0]['puesto']);

                $sheet->getStyle('B3')->getFont()->setBold(true);
                $sheet->setCellValue("B3","Edad:");
                $sheet->mergeCells("C3:E3");
                $sheet->setCellValue("C3",$data[0]['edad']);

                $sheet->getStyle('H3')->getFont()->setBold(true);
                $sheet->setCellValue("H3","Escolaridad:");
                $sheet->mergeCells("I3:K3");
                $sheet->setCellValue("I3",$data[0]['escolaridad']);

                $sheet->setWidth(array(
                    'A'     =>  2,
                    'B'     =>  20,
                    'C'     =>  4,
                    'D'     =>  4,
                    'E'     =>  20,
                    'F'     =>  4,
                    'G'     =>  4,
                    'H'     =>  24,
                    'I'     =>  4,
                    'J'     =>  4,
                    'K'     =>  20,
                    'L'     =>  4,
                    'M'     =>  4,
                    'N'     =>  2,
                    'O'     =>  2,
                    'P'     =>  6,
                    'Q'     =>  6,
                    'R'     =>  6,
                    'T'     =>  6
                ));

                $sheet->cells('A1:BZ100', function ($cells) {
                    $cells->setBackground('#ffffff');
                });

                $sheet->cells('B5:M34', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('C5:D34', function ($cells) {
                    $cells->setFontColor('#ff0000');
                    $cells->setBackground('#ffff00');
                });

                $sheet->cells('F5:G34', function ($cells) {
                    $cells->setFontColor('#ff0000');
                    $cells->setBackground('#ffff00');
                });

                $sheet->cells('I5:J34', function ($cells) {
                    $cells->setFontColor('#ff0000');
                    $cells->setBackground('#ffff00');
                });

                $sheet->cells('L5:M34', function ($cells) {
                    $cells->setFontColor('#ff0000');
                    $cells->setBackground('#ffff00');
                });

                $sheet->cells('B6:B34', function ($cells) {
                    $cells->setAlignment('left');
                });

                $sheet->cells('E6:E34', function ($cells) {
                    $cells->setAlignment('left');
                });

                $sheet->cells('H6:H34', function ($cells) {
                    $cells->setAlignment('left');
                });

                $sheet->cells('K6:K34', function ($cells) {
                    $cells->setAlignment('left');
                });

                $sheet->cells('B5:M5', function ($cells) {
                    $cells->setFontColor('#000000');
                    $cells->setBackground('#FFFFFF');
                });

                $ln = 6;
                $r=0;
                $x=0;
                $rowArray = [];

                $sheet->getStyle("C5:M5")->getFont()->setBold(true);
                $sheet->setCellValue("C5","M");
                $sheet->setCellValue("D5","L");
                $sheet->setCellValue("F5","M");
                $sheet->setCellValue("G5","L");
                $sheet->setCellValue("I5","M");
                $sheet->setCellValue("J5","L");
                $sheet->setCellValue("L5","M");
                $sheet->setCellValue("M5","L");

                $sheet->setCellValue("B6",'PERSUASIVO');
                $sheet->setCellValue("E6",'FZA DE VOLUNTAD');
                $sheet->setCellValue("H6",'OBEDIENTE');
                $sheet->setCellValue("K6",'AVENTURERO');
                $sheet->setCellValue("B7",'GENTIL');
                $sheet->setCellValue("E7",'MENTE ABIERTA');
                $sheet->setCellValue("H7",'QUISQUILLOSO');
                $sheet->setCellValue("K7",'RECEPTIVO');
                $sheet->setCellValue("B8",'HUMILDE');
                $sheet->setCellValue("E8",'COMPLACIENTE');
                $sheet->setCellValue("H8",'INCONQUISTABLE');
                $sheet->setCellValue("K8",'CORDIAL');
                $sheet->setCellValue("B9",'ORIGINAL');
                $sheet->setCellValue("E9",'ANIMOSO');
                $sheet->setCellValue("H9",'JUGUETON');
                $sheet->setCellValue("K9",'MODERADO');

                $sheet->setCellValue("B11",'AGRESIVO');
                $sheet->setCellValue("E11",'CONFIADO');
                $sheet->setCellValue("H11",'RESPETUOSO');
                $sheet->setCellValue("K11",'INDULGENTE');
                $sheet->setCellValue("B12",'ALMA DE LA FIESTA');
                $sheet->setCellValue("E12",'SIMPATIZADOR');
                $sheet->setCellValue("H12",'EMPRENDEDOR');
                $sheet->setCellValue("K12",'ESTETA');
                $sheet->setCellValue("B13",'COMODINO');
                $sheet->setCellValue("E13",'TOLERANTE');
                $sheet->setCellValue("H13",'OPTIMISTA');
                $sheet->setCellValue("K13",'VIGOROSO');
                $sheet->setCellValue("B14",'TEMEROSO');
                $sheet->setCellValue("E14",'AFIRMATIVO');
                $sheet->setCellValue("H14",'SERVICIAL');
                $sheet->setCellValue("K14",'SOCIABLE');

                $sheet->setCellValue("B16",'AGRADABLE');
                $sheet->setCellValue("E16",'ECUANIME');
                $sheet->setCellValue("H16",'VALIENTE');
                $sheet->setCellValue("K16",'PARLANCHIN');
                $sheet->setCellValue("B17",'TEMEROSO DE DIOS');
                $sheet->setCellValue("E17",'PRECISO');
                $sheet->setCellValue("H17",'INSPIRADOR');
                $sheet->setCellValue("K17",'CONTROLADO');
                $sheet->setCellValue("B18",'TENAZ');
                $sheet->setCellValue("E18",'NERVIOSO');
                $sheet->setCellValue("H18",'SUMISO');
                $sheet->setCellValue("K18",'CONVENCIONAL');
                $sheet->setCellValue("B19",'ATRACTIVO');
                $sheet->setCellValue("E19",'JOVIAL');
                $sheet->setCellValue("H19",'TÍMIDO');
                $sheet->setCellValue("K19",'DECISIVO');

                $sheet->setCellValue("B21",'CAUTELOSO');
                $sheet->setCellValue("E21",'DISCIPLINADO');
                $sheet->setCellValue("H21",'ADAPTABLE');
                $sheet->setCellValue("K21",'COHIBIDO');
                $sheet->setCellValue("B22",'DETERMINADO');
                $sheet->setCellValue("E22",'GENEROSO');
                $sheet->setCellValue("H22",'DISPUTADOR');
                $sheet->setCellValue("K22",'EXACTO');
                $sheet->setCellValue("B23",'CONVINCENTE');
                $sheet->setCellValue("E23",'ANIMOSO');
                $sheet->setCellValue("H23",'INDIFERENTE');
                $sheet->setCellValue("K23",'FRANCO');
                $sheet->setCellValue("B24",'BONACHON');
                $sheet->setCellValue("E24",'PERSISTENTE');
                $sheet->setCellValue("H24",'SANGRE LIVIANA');
                $sheet->setCellValue("K24",'BUEN COMPAÑERO');

                $sheet->setCellValue("B26",'DOCIL');
                $sheet->setCellValue("E26",'COMPETITIVO');
                $sheet->setCellValue("H26",'AMIGUERO');
                $sheet->setCellValue("K26",'DIPLOMÁTICO');
                $sheet->setCellValue("B27",'ATREVIDO');
                $sheet->setCellValue("E27",'ALEGRE');
                $sheet->setCellValue("H27",'PACIENTE');
                $sheet->setCellValue("K27",'AUDAZ');
                $sheet->setCellValue("B28",'LEAL');
                $sheet->setCellValue("E28",'CONSIDERADO');
                $sheet->setCellValue("H28",'CONFIANZA EN SI MISMO');
                $sheet->setCellValue("K28",'REFINADO');
                $sheet->setCellValue("B29",'ENCANTADOR');
                $sheet->setCellValue("E29",'ARMONIOSO');
                $sheet->setCellValue("H29",'MESURADO PARA HABLAR');
                $sheet->setCellValue("K29",'SATISFECHO');

                $sheet->setCellValue("B31",'DISPUESTO');
                $sheet->setCellValue("E31",'ADMIRABLE');
                $sheet->setCellValue("H31",'CONFORME');
                $sheet->setCellValue("K31",'INQUIETO');
                $sheet->setCellValue("B32",'DESEOSO');
                $sheet->setCellValue("E32",'BONDADOSO');
                $sheet->setCellValue("H32",'CONFIABLE');
                $sheet->setCellValue("K32",'POPULAR');
                $sheet->setCellValue("B33",'CONSECUENTE');
                $sheet->setCellValue("E33",'RESIGNADO');
                $sheet->setCellValue("H33",'PAFICICO');
                $sheet->setCellValue("K33",'BUEN VECINO');
                $sheet->setCellValue("B34",'ENTUSIASTA');
                $sheet->setCellValue("E34",'CARACTER FIRME');
                $sheet->setCellValue("H34",'POSITIVO');
                $sheet->setCellValue("K34",'DEVOTO');

                for ($i=0; $i < 16 ; $i++) {
                    if (($i+1)==1) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("C6",1);
                            $sheet->setCellValue("D6",0);
                        }else{
                            $sheet->setCellValue("C6",0);
                            $sheet->setCellValue("D6",1);
                        }
                    }else if (($i+1)==2) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("F6",1);
                            $sheet->setCellValue("G6",0);
                        }else{
                            $sheet->setCellValue("F6",0);
                            $sheet->setCellValue("G6",1);
                        }
                    }else if (($i+1)==3) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("I6",1);
                            $sheet->setCellValue("J6",0);
                        }else{
                            $sheet->setCellValue("I6",0);
                            $sheet->setCellValue("J6",1);
                        }
                    }else if (($i+1)==4) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("M6",1);
                            $sheet->setCellValue("L6",0);
                        }else{
                            $sheet->setCellValue("M6",0);
                            $sheet->setCellValue("L6",1);
                        }
                    }else if (($i+1)==5) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("C7",1);
                            $sheet->setCellValue("D7",0);
                        }else{
                            $sheet->setCellValue("C7",0);
                            $sheet->setCellValue("D7",1);
                        }
                    }else if (($i+1)==6) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("F7",1);
                            $sheet->setCellValue("G7",0);
                        }else{
                            $sheet->setCellValue("F7",0);
                            $sheet->setCellValue("G7",1);
                        }
                    }else if (($i+1)==7) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("I7",1);
                            $sheet->setCellValue("J7",0);
                        }else{
                            $sheet->setCellValue("I7",0);
                            $sheet->setCellValue("J7",1);
                        }
                    }else if (($i+1)==8) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("M7",1);
                            $sheet->setCellValue("L7",0);
                        }else{
                            $sheet->setCellValue("M7",0);
                            $sheet->setCellValue("L7",1);
                        }
                    }else if (($i+1)==9) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("C8",1);
                            $sheet->setCellValue("D8",0);
                        }else{
                            $sheet->setCellValue("C8",0);
                            $sheet->setCellValue("D8",1);
                        }
                    }else if (($i+1)==10) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("F8",1);
                            $sheet->setCellValue("G8",0);
                        }else{
                            $sheet->setCellValue("F8",0);
                            $sheet->setCellValue("G8",1);
                        }
                    }else if (($i+1)==11) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("I8",1);
                            $sheet->setCellValue("J8",0);
                        }else{
                            $sheet->setCellValue("I8",0);
                            $sheet->setCellValue("J8",1);
                        }
                    }else if (($i+1)==12) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("M8",1);
                            $sheet->setCellValue("L8",0);
                        }else{
                            $sheet->setCellValue("M8",0);
                            $sheet->setCellValue("L8",1);
                        }
                    }else if (($i+1)==13) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("C9",1);
                            $sheet->setCellValue("D9",0);
                        }else{
                            $sheet->setCellValue("C9",0);
                            $sheet->setCellValue("D9",1);
                        }
                    }else if (($i+1)==14) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("F9",1);
                            $sheet->setCellValue("G9",0);
                        }else{
                            $sheet->setCellValue("F9",0);
                            $sheet->setCellValue("G9",1);
                        }
                    }else if (($i+1)==15) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("I9",1);
                            $sheet->setCellValue("J9",0);
                        }else{
                            $sheet->setCellValue("I9",0);
                            $sheet->setCellValue("J9",1);
                        }
                    }else if (($i+1)==16) {
                        if ($data[0]['resp'.($i+1)] == 1) {
                            $sheet->setCellValue("M9",1);
                            $sheet->setCellValue("L9",0);
                        }else{
                            $sheet->setCellValue("M9",0);
                            $sheet->setCellValue("L9",1);
                        }
                    }
                }

                $j = 0;
                for ($i=17; $i <= 32 ; $i++) {
                    if (($i)==17) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C11",1);
                            $sheet->setCellValue("D11",0);
                        }else{
                            $sheet->setCellValue("C11",0);
                            $sheet->setCellValue("D11",1);
                        }
                    }else if (($i)==18) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F11",1);
                            $sheet->setCellValue("G11",0);
                        }else{
                            $sheet->setCellValue("F11",0);
                            $sheet->setCellValue("G11",1);
                        }
                    }else if (($i)==19) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I11",1);
                            $sheet->setCellValue("J11",0);
                        }else{
                            $sheet->setCellValue("I11",0);
                            $sheet->setCellValue("J11",1);
                        }
                    }else if (($i)==20) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L11",1);
                            $sheet->setCellValue("M11",0);
                        }else{
                            $sheet->setCellValue("L11",0);
                            $sheet->setCellValue("M11",1);
                        }
                    }else if (($i)==21) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C12",1);
                            $sheet->setCellValue("D12",0);
                        }else{
                            $sheet->setCellValue("C12",0);
                            $sheet->setCellValue("D12",1);
                        }
                    }else if (($i)==22) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F12",1);
                            $sheet->setCellValue("G12",0);
                        }else{
                            $sheet->setCellValue("F12",0);
                            $sheet->setCellValue("G12",1);
                        }
                    }else if (($i)==23) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I12",1);
                            $sheet->setCellValue("J12",0);
                        }else{
                            $sheet->setCellValue("I12",0);
                            $sheet->setCellValue("J12",1);
                        }
                    }else if (($i)==24) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L12",1);
                            $sheet->setCellValue("M12",0);
                        }else{
                            $sheet->setCellValue("L12",0);
                            $sheet->setCellValue("M12",1);
                        }
                    }else if (($i)==25) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C13",1);
                            $sheet->setCellValue("D13",0);
                        }else{
                            $sheet->setCellValue("C13",0);
                            $sheet->setCellValue("D13",1);
                        }
                    }else if (($i)==26) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F13",1);
                            $sheet->setCellValue("G13",0);
                        }else{
                            $sheet->setCellValue("F13",0);
                            $sheet->setCellValue("G13",1);
                        }
                    }else if (($i)==27) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I13",1);
                            $sheet->setCellValue("J13",0);
                        }else{
                            $sheet->setCellValue("I13",0);
                            $sheet->setCellValue("J13",1);
                        }
                    }else if (($i)==28) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L13",1);
                            $sheet->setCellValue("M13",0);
                        }else{
                            $sheet->setCellValue("L13",0);
                            $sheet->setCellValue("M13",1);
                        }
                    }else if (($i)==29) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C14",1);
                            $sheet->setCellValue("D14",0);
                        }else{
                            $sheet->setCellValue("C14",0);
                            $sheet->setCellValue("D14",1);
                        }
                    }else if (($i)==30) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F14",1);
                            $sheet->setCellValue("G14",0);
                        }else{
                            $sheet->setCellValue("F14",0);
                            $sheet->setCellValue("G14",1);
                        }
                    }else if (($i)==31) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I14",1);
                            $sheet->setCellValue("J14",0);
                        }else{
                            $sheet->setCellValue("I14",0);
                            $sheet->setCellValue("J14",1);
                        }
                    }else if (($i)==32) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L14",1);
                            $sheet->setCellValue("M14",0);
                        }else{
                            $sheet->setCellValue("L14",0);
                            $sheet->setCellValue("M14",1);
                        }
                    }
                    $j++;
                }

                $j = 0;
                for ($i=33; $i <= 48 ; $i++) {
                    if (($i)==33) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C16",1);
                            $sheet->setCellValue("D16",0);
                        }else{
                            $sheet->setCellValue("C16",0);
                            $sheet->setCellValue("D16",1);
                        }
                    }else if (($i)==34) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F16",1);
                            $sheet->setCellValue("G16",0);
                        }else{
                            $sheet->setCellValue("F16",0);
                            $sheet->setCellValue("G16",1);
                        }
                    }else if (($i)==35) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I16",1);
                            $sheet->setCellValue("J16",0);
                        }else{
                            $sheet->setCellValue("I16",0);
                            $sheet->setCellValue("J16",1);
                        }
                    }else if (($i)==36) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L16",1);
                            $sheet->setCellValue("M16",0);
                        }else{
                            $sheet->setCellValue("L16",0);
                            $sheet->setCellValue("M16",1);
                        }
                    }else if (($i)==37) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C17",1);
                            $sheet->setCellValue("D17",0);
                        }else{
                            $sheet->setCellValue("C17",0);
                            $sheet->setCellValue("D17",1);
                        }
                    }else if (($i)==38) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F17",1);
                            $sheet->setCellValue("G17",0);
                        }else{
                            $sheet->setCellValue("F17",0);
                            $sheet->setCellValue("G17",1);
                        }
                    }else if (($i)==39) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I17",1);
                            $sheet->setCellValue("J17",0);
                        }else{
                            $sheet->setCellValue("I17",0);
                            $sheet->setCellValue("J17",1);
                        }
                    }else if (($i)==40) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L17",1);
                            $sheet->setCellValue("M17",0);
                        }else{
                            $sheet->setCellValue("L17",0);
                            $sheet->setCellValue("M17",1);
                        }
                    }else if (($i)==41) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C18",1);
                            $sheet->setCellValue("D18",0);
                        }else{
                            $sheet->setCellValue("C18",0);
                            $sheet->setCellValue("D18",1);
                        }
                    }else if (($i)==42) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F18",1);
                            $sheet->setCellValue("G18",0);
                        }else{
                            $sheet->setCellValue("F18",0);
                            $sheet->setCellValue("G18",1);
                        }
                    }else if (($i)==43) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I18",1);
                            $sheet->setCellValue("J18",0);
                        }else{
                            $sheet->setCellValue("I18",0);
                            $sheet->setCellValue("J18",1);
                        }
                    }else if (($i)==44) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L18",1);
                            $sheet->setCellValue("M18",0);
                        }else{
                            $sheet->setCellValue("L18",0);
                            $sheet->setCellValue("M18",1);
                        }
                    }else if (($i)==45) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C19",1);
                            $sheet->setCellValue("D19",0);
                        }else{
                            $sheet->setCellValue("C19",0);
                            $sheet->setCellValue("D19",1);
                        }
                    }else if (($i)==46) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F19",1);
                            $sheet->setCellValue("G19",0);
                        }else{
                            $sheet->setCellValue("F19",0);
                            $sheet->setCellValue("G19",1);
                        }
                    }else if (($i)==47) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I19",1);
                            $sheet->setCellValue("J19",0);
                        }else{
                            $sheet->setCellValue("I19",0);
                            $sheet->setCellValue("J19",1);
                        }
                    }else if (($i)==48) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L19",1);
                            $sheet->setCellValue("M19",0);
                        }else{
                            $sheet->setCellValue("L19",0);
                            $sheet->setCellValue("M19",1);
                        }
                    }
                    $j++;
                }

                $j = 0;
                for ($i=49; $i <= 64 ; $i++) {
                    if (($i)==49) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C21",1);
                            $sheet->setCellValue("D21",0);
                        }else{
                            $sheet->setCellValue("C21",0);
                            $sheet->setCellValue("D21",1);
                        }
                    }else if (($i)==50) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F21",1);
                            $sheet->setCellValue("G21",0);
                        }else{
                            $sheet->setCellValue("F21",0);
                            $sheet->setCellValue("G21",1);
                        }
                    }else if (($i)==51) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I21",1);
                            $sheet->setCellValue("J21",0);
                        }else{
                            $sheet->setCellValue("I21",0);
                            $sheet->setCellValue("J21",1);
                        }
                    }else if (($i)==52) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L21",1);
                            $sheet->setCellValue("M21",0);
                        }else{
                            $sheet->setCellValue("L21",0);
                            $sheet->setCellValue("M21",1);
                        }
                    }else if (($i)==53) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C22",1);
                            $sheet->setCellValue("D22",0);
                        }else{
                            $sheet->setCellValue("C22",0);
                            $sheet->setCellValue("D22",1);
                        }
                    }else if (($i)==54) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F22",1);
                            $sheet->setCellValue("G22",0);
                        }else{
                            $sheet->setCellValue("F22",0);
                            $sheet->setCellValue("G22",1);
                        }
                    }else if (($i)==55) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I22",1);
                            $sheet->setCellValue("J22",0);
                        }else{
                            $sheet->setCellValue("I22",0);
                            $sheet->setCellValue("J22",1);
                        }
                    }else if (($i)==56) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L22",1);
                            $sheet->setCellValue("M22",0);
                        }else{
                            $sheet->setCellValue("L22",0);
                            $sheet->setCellValue("M22",1);
                        }
                    }else if (($i)==57) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C23",1);
                            $sheet->setCellValue("D23",0);
                        }else{
                            $sheet->setCellValue("C23",0);
                            $sheet->setCellValue("D23",1);
                        }
                    }else if (($i)==58) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F23",1);
                            $sheet->setCellValue("G23",0);
                        }else{
                            $sheet->setCellValue("F23",0);
                            $sheet->setCellValue("G23",1);
                        }
                    }else if (($i)==59) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I23",1);
                            $sheet->setCellValue("J23",0);
                        }else{
                            $sheet->setCellValue("I23",0);
                            $sheet->setCellValue("J23",1);
                        }
                    }else if (($i)==60) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L23",1);
                            $sheet->setCellValue("M23",0);
                        }else{
                            $sheet->setCellValue("L23",0);
                            $sheet->setCellValue("M23",1);
                        }
                    }else if (($i)==61) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C24",1);
                            $sheet->setCellValue("D24",0);
                        }else{
                            $sheet->setCellValue("C24",0);
                            $sheet->setCellValue("D24",1);
                        }
                    }else if (($i)==62) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F24",1);
                            $sheet->setCellValue("G24",0);
                        }else{
                            $sheet->setCellValue("F24",0);
                            $sheet->setCellValue("G24",1);
                        }
                    }else if (($i)==63) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I24",1);
                            $sheet->setCellValue("J24",0);
                        }else{
                            $sheet->setCellValue("I24",0);
                            $sheet->setCellValue("J24",1);
                        }
                    }else if (($i)==64) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L24",1);
                            $sheet->setCellValue("M24",0);
                        }else{
                            $sheet->setCellValue("L24",0);
                            $sheet->setCellValue("M24",1);
                        }
                    }
                    $j++;
                }

                $j = 0;
                for ($i=65; $i <= 80 ; $i++) {
                    if (($i)==65) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C26",1);
                            $sheet->setCellValue("D26",0);
                        }else{
                            $sheet->setCellValue("C26",0);
                            $sheet->setCellValue("D26",1);
                        }
                    }else if (($i)==66) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F26",1);
                            $sheet->setCellValue("G26",0);
                        }else{
                            $sheet->setCellValue("F26",0);
                            $sheet->setCellValue("G26",1);
                        }
                    }else if (($i)==67) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I26",1);
                            $sheet->setCellValue("J26",0);
                        }else{
                            $sheet->setCellValue("I26",0);
                            $sheet->setCellValue("J26",1);
                        }
                    }else if (($i)==68) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L26",1);
                            $sheet->setCellValue("M26",0);
                        }else{
                            $sheet->setCellValue("L26",0);
                            $sheet->setCellValue("M26",1);
                        }
                    }else if (($i)==69) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C27",1);
                            $sheet->setCellValue("D27",0);
                        }else{
                            $sheet->setCellValue("C27",0);
                            $sheet->setCellValue("D27",1);
                        }
                    }else if (($i)==70) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F27",1);
                            $sheet->setCellValue("G27",0);
                        }else{
                            $sheet->setCellValue("F27",0);
                            $sheet->setCellValue("G27",1);
                        }
                    }else if (($i)==71) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I27",1);
                            $sheet->setCellValue("J27",0);
                        }else{
                            $sheet->setCellValue("I27",0);
                            $sheet->setCellValue("J27",1);
                        }
                    }else if (($i)==72) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L27",1);
                            $sheet->setCellValue("M27",0);
                        }else{
                            $sheet->setCellValue("L27",0);
                            $sheet->setCellValue("M27",1);
                        }
                    }else if (($i)==73) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C28",1);
                            $sheet->setCellValue("D28",0);
                        }else{
                            $sheet->setCellValue("C28",0);
                            $sheet->setCellValue("D28",1);
                        }
                    }else if (($i)==74) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F28",1);
                            $sheet->setCellValue("G28",0);
                        }else{
                            $sheet->setCellValue("F28",0);
                            $sheet->setCellValue("G28",1);
                        }
                    }else if (($i)==75) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I28",1);
                            $sheet->setCellValue("J28",0);
                        }else{
                            $sheet->setCellValue("I28",0);
                            $sheet->setCellValue("J28",1);
                        }
                    }else if (($i)==76) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L28",1);
                            $sheet->setCellValue("M28",0);
                        }else{
                            $sheet->setCellValue("L28",0);
                            $sheet->setCellValue("M28",1);
                        }
                    }else if (($i)==77) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C29",1);
                            $sheet->setCellValue("D29",0);
                        }else{
                            $sheet->setCellValue("C29",0);
                            $sheet->setCellValue("D29",1);
                        }
                    }else if (($i)==78) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F29",1);
                            $sheet->setCellValue("G29",0);
                        }else{
                            $sheet->setCellValue("F29",0);
                            $sheet->setCellValue("G29",1);
                        }
                    }else if (($i)==79) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I29",1);
                            $sheet->setCellValue("J29",0);
                        }else{
                            $sheet->setCellValue("I29",0);
                            $sheet->setCellValue("J29",1);
                        }
                    }else if (($i)==80) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L29",1);
                            $sheet->setCellValue("M29",0);
                        }else{
                            $sheet->setCellValue("L29",0);
                            $sheet->setCellValue("M29",1);
                        }
                    }
                    $j++;
                }

                $j = 0;
                for ($i=81; $i <= 96 ; $i++) {
                    if (($i)==81) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C31",1);
                            $sheet->setCellValue("D31",0);
                        }else{
                            $sheet->setCellValue("C31",0);
                            $sheet->setCellValue("D31",1);
                        }
                    }else if (($i)==82) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F31",1);
                            $sheet->setCellValue("G31",0);
                        }else{
                            $sheet->setCellValue("F31",0);
                            $sheet->setCellValue("G31",1);
                        }
                    }else if (($i)==83) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I31",1);
                            $sheet->setCellValue("J31",0);
                        }else{
                            $sheet->setCellValue("I31",0);
                            $sheet->setCellValue("J31",1);
                        }
                    }else if (($i)==84) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L31",1);
                            $sheet->setCellValue("M31",0);
                        }else{
                            $sheet->setCellValue("L31",0);
                            $sheet->setCellValue("M31",1);
                        }
                    }else if (($i)==85) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C32",1);
                            $sheet->setCellValue("D32",0);
                        }else{
                            $sheet->setCellValue("C32",0);
                            $sheet->setCellValue("D32",1);
                        }
                    }else if (($i)==86) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F32",1);
                            $sheet->setCellValue("G32",0);
                        }else{
                            $sheet->setCellValue("F32",0);
                            $sheet->setCellValue("G32",1);
                        }
                    }else if (($i)==87) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I32",1);
                            $sheet->setCellValue("J32",0);
                        }else{
                            $sheet->setCellValue("I32",0);
                            $sheet->setCellValue("J32",1);
                        }
                    }else if (($i)==88) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L32",1);
                            $sheet->setCellValue("M32",0);
                        }else{
                            $sheet->setCellValue("L32",0);
                            $sheet->setCellValue("M32",1);
                        }
                    }else if (($i)==89) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C33",1);
                            $sheet->setCellValue("D33",0);
                        }else{
                            $sheet->setCellValue("C33",0);
                            $sheet->setCellValue("D33",1);
                        }
                    }else if (($i)==90) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F33",1);
                            $sheet->setCellValue("G33",0);
                        }else{
                            $sheet->setCellValue("F33",0);
                            $sheet->setCellValue("G33",1);
                        }
                    }else if (($i)==91) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I33",1);
                            $sheet->setCellValue("J33",0);
                        }else{
                            $sheet->setCellValue("I33",0);
                            $sheet->setCellValue("J33",1);
                        }
                    }else if (($i)==92) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L33",1);
                            $sheet->setCellValue("M33",0);
                        }else{
                            $sheet->setCellValue("L33",0);
                            $sheet->setCellValue("M33",1);
                        }
                    }else if (($i)==93) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("C34",1);
                            $sheet->setCellValue("D34",0);
                        }else{
                            $sheet->setCellValue("C34",0);
                            $sheet->setCellValue("D34",1);
                        }
                    }else if (($i)==94) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("F34",1);
                            $sheet->setCellValue("G34",0);
                        }else{
                            $sheet->setCellValue("F34",0);
                            $sheet->setCellValue("G34",1);
                        }
                    }else if (($i)==95) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("I34",1);
                            $sheet->setCellValue("J34",0);
                        }else{
                            $sheet->setCellValue("I34",0);
                            $sheet->setCellValue("J34",1);
                        }
                    }else if (($i)==96) {
                        if ($data[0]['resp'.$i] == 1) {
                            $sheet->setCellValue("L34",1);
                            $sheet->setCellValue("M34",0);
                        }else{
                            $sheet->setCellValue("L34",0);
                            $sheet->setCellValue("M34",1);
                        }
                    }
                    $j++;
                }

                $sheet->cells('P6:T9', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('Q7:T9', function ($cells) {
                    $cells->setFontColor('#ff0000');
                });

                $sheet->cells('P12:T15', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('P6:T6', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('P6:P9', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('P12:T12', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->cells('P12:P15', function ($cells) {
                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                    $cells->setAlignment('center');
                });

                $sheet->getStyle("Q6:T6")->getFont()->setBold(true);
                $sheet->setCellValue("Q6","D");
                $sheet->setCellValue("R6","I");
                $sheet->setCellValue("S6","S");
                $sheet->setCellValue("T6","C");
                $sheet->getStyle("P6:P9")->getFont()->setBold(true);
                $sheet->setCellValue("P7","M");
                $sheet->setCellValue("P8","L");
                $sheet->setCellValue("P9","TOT");

                $sheet->setCellValue("Q7","=C11+C18+C22+C27+F14+F24+F26+F34+I8+I12+I16+I22+I28+I34+L6+L13+L19+L23+L27+L31");
                $sheet->setCellValue("Q8","=D9+D18+D27+D34+G6+G14+G18+G24+G26+G34+J8+J12+J22+J28+J34+M6+M13+M19+M23+M27+M31");
                $sheet->setCellValue("Q9","=Q7-Q8");

                $sheet->setCellValue("R7","=C6+C12+C19+C23+C29+F9+F11+F31+I9+I13+I17+I24+I26+L8+L14+L16+L24+L32");
                $sheet->setCellValue("R8","=D12+D19+D23+D29+G9+G19+G23+G27+J9+J13+J24+J26+J32+M8+M14+M16+M24+M28+M32");
                $sheet->setCellValue("R9","=R7-R8");

                $sheet->setCellValue("S7","=C7+C13+C24+C28+C31+F8+F16+F22+F28+F32+I6+I14+I27+I32+L9+L11+L17+L29+L33");
                $sheet->setCellValue("S8","=D7+D13+D16+G8+G12+G16+G22+G28+J14+J18+J23+J27+J31+M9+M11+M17+M21+M29+M33");
                $sheet->setCellValue("S9","=S7-S8");

                $sheet->setCellValue("T7","=C8+C17+C21+C33+F7+F17+F21+I11+I21+I29+I33+L7+L22+L26+L34");
                $sheet->setCellValue("T8","=D8+D14+D17+D21+D26+D33+G13+G17+G29+G33+J7+J19+J33+M12+M18+M34");
                $sheet->setCellValue("T9","=T7-T8");

                $sheet->getStyle("Q12:T12")->getFont()->setBold(true);
                $sheet->setCellValue("Q12","D");
                $sheet->setCellValue("R12","I");
                $sheet->setCellValue("S12","S");
                $sheet->setCellValue("T12","C");
                $sheet->getStyle("P12:P15")->getFont()->setBold(true);
                $sheet->setCellValue("P13","M");
                $sheet->setCellValue("P14","L");
                $sheet->setCellValue("P15","TOT");

                $sheet->cells('V1:BD44', function ($cells) {
                    $cells->setAlignment('center');
                });

                $sheet->setCellValue("V1","D (M)");
                for ($i=0; $i <= 20 ; $i++) { 
                    $sheet->setCellValue("V".($i+3),$i);
                }
                
                $sheet->setCellValue("W1","%");
                $sheet->setCellValue("W3",'1');
                $sheet->setCellValue("W4",'5');
                $sheet->setCellValue("W5",'10');
                $sheet->setCellValue("W6",'20');
                $sheet->setCellValue("W7",'30');
                $sheet->setCellValue("W8",'40');
                $sheet->setCellValue("W9",'50');
                $sheet->setCellValue("W10",'60');
                $sheet->setCellValue("W11",'65');
                $sheet->setCellValue("W12",'75');
                $sheet->setCellValue("W13",'84');
                $sheet->setCellValue("W14",'87');
                $sheet->setCellValue("W15",'90');
                $sheet->setCellValue("W16",'93');
                $sheet->setCellValue("W17",'95');
                $sheet->setCellValue("W18",'97');
                $sheet->setCellValue("W19",'97');
                $sheet->setCellValue("W20",'98');
                $sheet->setCellValue("W21",'98');
                $sheet->setCellValue("W22",'98');
                $sheet->setCellValue("W23",'99');

                $sheet->setCellValue("Y1","I (M)");
                for ($i=0; $i <= 17 ; $i++) { 
                    $sheet->setCellValue("Y".($i+3),$i);
                }

                $sheet->setCellValue("Z1","%");
                $sheet->setCellValue("Z3",'4');
                $sheet->setCellValue("Z4",'10');
                $sheet->setCellValue("Z5",'25');
                $sheet->setCellValue("Z6",'40');
                $sheet->setCellValue("Z7",'55');
                $sheet->setCellValue("Z8",'70');
                $sheet->setCellValue("Z9",'82');
                $sheet->setCellValue("Z10",'90');
                $sheet->setCellValue("Z11",'95');
                $sheet->setCellValue("Z12",'96');
                $sheet->setCellValue("Z13",'97');
                $sheet->setCellValue("Z14",'97');
                $sheet->setCellValue("Z15",'97');
                $sheet->setCellValue("Z16",'97');
                $sheet->setCellValue("Z17",'97');
                $sheet->setCellValue("Z18",'97');
                $sheet->setCellValue("Z19",'97');
                $sheet->setCellValue("Z20",'99');

                $sheet->setCellValue("AB1","S (M)");
                for ($i=0; $i <= 19 ; $i++) { 
                    $sheet->setCellValue("AB".($i+3),$i);
                }

                $sheet->setCellValue("AC1","%");
                $sheet->setCellValue("AC3",'5');
                $sheet->setCellValue("AC4",'10');
                $sheet->setCellValue("AC5",'16');
                $sheet->setCellValue("AC6",'30');
                $sheet->setCellValue("AC7",'40');
                $sheet->setCellValue("AC8",'55');
                $sheet->setCellValue("AC9",'63');
                $sheet->setCellValue("AC10",'75');
                $sheet->setCellValue("AC11",'84');
                $sheet->setCellValue("AC12",'90');
                $sheet->setCellValue("AC13",'95');
                $sheet->setCellValue("AC14",'96');
                $sheet->setCellValue("AC15",'97');
                $sheet->setCellValue("AC16",'97');
                $sheet->setCellValue("AC17",'97');
                $sheet->setCellValue("AC18",'97');
                $sheet->setCellValue("AC19",'98');
                $sheet->setCellValue("AC20",'98');
                $sheet->setCellValue("AC21",'98');
                $sheet->setCellValue("AC22",'99');

                $sheet->setCellValue("AE1","C (M)");
                for ($i=0; $i <= 15 ; $i++) { 
                    $sheet->setCellValue("AE".($i+3),$i);
                }

                $sheet->setCellValue("AF1","%");
                $sheet->setCellValue("AF3",'1');
                $sheet->setCellValue("AF4",'5');
                $sheet->setCellValue("AF5",'16');
                $sheet->setCellValue("AF6",'30');
                $sheet->setCellValue("AF7",'55');
                $sheet->setCellValue("AF8",'70');
                $sheet->setCellValue("AF9",'84');
                $sheet->setCellValue("AF10",'93');
                $sheet->setCellValue("AF11",'95');
                $sheet->setCellValue("AF12",'97');
                $sheet->setCellValue("AF13",'97');
                $sheet->setCellValue("AF14",'97');
                $sheet->setCellValue("AF15",'98');
                $sheet->setCellValue("AF16",'98');
                $sheet->setCellValue("AF17",'98');
                $sheet->setCellValue("AF18",'99');

                $sheet->setCellValue("AH1","D (L)");
                for ($i=0; $i <= 21 ; $i++) { 
                    $sheet->setCellValue("AH".($i+3),$i);
                }

                $sheet->setCellValue("AI1","%");
                $sheet->setCellValue("AI3",'99');
                $sheet->setCellValue("AI4",'95');
                $sheet->setCellValue("AI5",'87');
                $sheet->setCellValue("AI6",'80');
                $sheet->setCellValue("AI7",'65');
                $sheet->setCellValue("AI8",'55');
                $sheet->setCellValue("AI9",'50');
                $sheet->setCellValue("AI10",'35');
                $sheet->setCellValue("AI11",'30');
                $sheet->setCellValue("AI12",'20');
                $sheet->setCellValue("AI13",'18');
                $sheet->setCellValue("AI14",'15');
                $sheet->setCellValue("AI15",'10');
                $sheet->setCellValue("AI16",'6');
                $sheet->setCellValue("AI17",'5');
                $sheet->setCellValue("AI18",'4');
                $sheet->setCellValue("AI19",'3');
                $sheet->setCellValue("AI20",'2');
                $sheet->setCellValue("AI21",'2');
                $sheet->setCellValue("AI22",'2');
                $sheet->setCellValue("AI23",'2');
                $sheet->setCellValue("AI24",'1');

                $sheet->setCellValue("AK1","D (L)");
                for ($i=0; $i <= 19 ; $i++) { 
                    $sheet->setCellValue("AK".($i+3),$i);
                }

                $sheet->setCellValue("AL1","%");
                $sheet->setCellValue("AL3",'99');
                $sheet->setCellValue("AL4",'95');
                $sheet->setCellValue("AL5",'87');
                $sheet->setCellValue("AL6",'75');
                $sheet->setCellValue("AL7",'55');
                $sheet->setCellValue("AL8",'40');
                $sheet->setCellValue("AL9",'25');
                $sheet->setCellValue("AL10",'16');
                $sheet->setCellValue("AL11",'10');
                $sheet->setCellValue("AL12",'5');
                $sheet->setCellValue("AL13",'4');
                $sheet->setCellValue("AL14",'4');
                $sheet->setCellValue("AL15",'3');
                $sheet->setCellValue("AL16",'3');
                $sheet->setCellValue("AL17",'3');
                $sheet->setCellValue("AL18",'2');
                $sheet->setCellValue("AL19",'2');
                $sheet->setCellValue("AL20",'2');
                $sheet->setCellValue("AL21",'2');
                $sheet->setCellValue("AL22",'1');

                $sheet->setCellValue("AN1","S (L)");
                for ($i=0; $i <= 19 ; $i++) { 
                    $sheet->setCellValue("AN".($i+3),$i);
                }

                $sheet->setCellValue("AO1","%");
                $sheet->setCellValue("AO3",'99');
                $sheet->setCellValue("AO4",'95');
                $sheet->setCellValue("AO5",'87');
                $sheet->setCellValue("AO6",'75');
                $sheet->setCellValue("AO7",'55');
                $sheet->setCellValue("AO8",'40');
                $sheet->setCellValue("AO9",'25');
                $sheet->setCellValue("AO10",'16');
                $sheet->setCellValue("AO11",'10');
                $sheet->setCellValue("AO12",'5');
                $sheet->setCellValue("AO13",'4');
                $sheet->setCellValue("AO14",'4');
                $sheet->setCellValue("AO15",'3');
                $sheet->setCellValue("AO16",'3');
                $sheet->setCellValue("AO17",'3');
                $sheet->setCellValue("AO18",'2');
                $sheet->setCellValue("AO19",'2');
                $sheet->setCellValue("AO20",'2');
                $sheet->setCellValue("AO21",'2');
                $sheet->setCellValue("AO22",'1');

                $sheet->setCellValue("AQ1","C (L)");
                for ($i=0; $i <= 16 ; $i++) { 
                    $sheet->setCellValue("AQ".($i+3),$i);
                }

                $sheet->setCellValue("AR1","%");
                $sheet->setCellValue("AR3",'99');
                $sheet->setCellValue("AR4",'97');
                $sheet->setCellValue("AR5",'95');
                $sheet->setCellValue("AR6",'90');
                $sheet->setCellValue("AR7",'84');
                $sheet->setCellValue("AR8",'70');
                $sheet->setCellValue("AR9",'55');
                $sheet->setCellValue("AR10",'40');
                $sheet->setCellValue("AR11",'38');
                $sheet->setCellValue("AR12",'23');
                $sheet->setCellValue("AR13",'10');
                $sheet->setCellValue("AR14",'5');
                $sheet->setCellValue("AR15",'4');
                $sheet->setCellValue("AR16",'3');
                $sheet->setCellValue("AR17",'2');
                $sheet->setCellValue("AR18",'2');
                $sheet->setCellValue("AR19",'1');

                $sheet->setCellValue("AT1","D (T)");
                $x= -21;
                for ($i=0; $i <= 41 ; $i++) { 
                    $sheet->setCellValue("AT".($i+3),$x);
                    $x++;
                }

                $sheet->setCellValue("AU1","%");
                $sheet->setCellValue("AU3",'1');
                $sheet->setCellValue("AU4",'2');
                $sheet->setCellValue("AU5",'2');
                $sheet->setCellValue("AU6",'2');
                $sheet->setCellValue("AU7",'2');
                $sheet->setCellValue("AU8",'2');
                $sheet->setCellValue("AU9",'2');
                $sheet->setCellValue("AU10",'2');
                $sheet->setCellValue("AU11",'4');
                $sheet->setCellValue("AU12",'5');
                $sheet->setCellValue("AU13",'5');
                $sheet->setCellValue("AU14",'9');
                $sheet->setCellValue("AU15",'13');
                $sheet->setCellValue("AU16",'15');
                $sheet->setCellValue("AU17",'16');
                $sheet->setCellValue("AU18",'20');
                $sheet->setCellValue("AU19",'25');
                $sheet->setCellValue("AU20",'29');
                $sheet->setCellValue("AU21",'35');
                $sheet->setCellValue("AU22",'40');
                $sheet->setCellValue("AU23",'45');
                $sheet->setCellValue("AU24",'50');
                $sheet->setCellValue("AU25",'55');
                $sheet->setCellValue("AU26",'60');
                $sheet->setCellValue("AU27",'65');
                $sheet->setCellValue("AU28",'67');
                $sheet->setCellValue("AU29",'70');
                $sheet->setCellValue("AU30",'75');
                $sheet->setCellValue("AU31",'80');
                $sheet->setCellValue("AU32",'84');
                $sheet->setCellValue("AU33",'85');
                $sheet->setCellValue("AU34",'90');
                $sheet->setCellValue("AU35",'91');
                $sheet->setCellValue("AU36",'94');
                $sheet->setCellValue("AU37",'95');
                $sheet->setCellValue("AU38",'96');
                $sheet->setCellValue("AU39",'97');
                $sheet->setCellValue("AU40",'97');
                $sheet->setCellValue("AU41",'98');
                $sheet->setCellValue("AU42",'98');
                $sheet->setCellValue("AU43",'98');
                $sheet->setCellValue("AU44",'99');

                $sheet->setCellValue("AW1","I (T)");
                $x= -19;
                for ($i=0; $i <= 36 ; $i++) { 
                    $sheet->setCellValue("AW".($i+3),$x);
                    $x++;
                }

                $sheet->setCellValue("AX1","%");
                $sheet->setCellValue("AX3",'1');
                $sheet->setCellValue("AX4",'2');
                $sheet->setCellValue("AX5",'2');
                $sheet->setCellValue("AX6",'2');
                $sheet->setCellValue("AX7",'2');
                $sheet->setCellValue("AX8",'2');
                $sheet->setCellValue("AX9",'2');
                $sheet->setCellValue("AX10",'2');
                $sheet->setCellValue("AX11",'2');
                $sheet->setCellValue("AX12",'3');
                $sheet->setCellValue("AX13",'4');
                $sheet->setCellValue("AX14",'5');
                $sheet->setCellValue("AX15",'6');
                $sheet->setCellValue("AX16",'10');
                $sheet->setCellValue("AX17",'16');
                $sheet->setCellValue("AX18",'20');
                $sheet->setCellValue("AX19",'29');
                $sheet->setCellValue("AX20",'35');
                $sheet->setCellValue("AX21",'45');
                $sheet->setCellValue("AX22",'55');
                $sheet->setCellValue("AX23",'60');
                $sheet->setCellValue("AX24",'70');
                $sheet->setCellValue("AX25",'75');
                $sheet->setCellValue("AX26",'85');
                $sheet->setCellValue("AX27",'90');
                $sheet->setCellValue("AX28",'95');
                $sheet->setCellValue("AX29",'96');
                $sheet->setCellValue("AX30",'97');
                $sheet->setCellValue("AX31",'97');
                $sheet->setCellValue("AX32",'98');
                $sheet->setCellValue("AX33",'98');
                $sheet->setCellValue("AX34",'98');
                $sheet->setCellValue("AX35",'98');
                $sheet->setCellValue("AX36",'98');
                $sheet->setCellValue("AX37",'98');
                $sheet->setCellValue("AX38",'98');
                $sheet->setCellValue("AX39",'99');

                $sheet->setCellValue("AZ1","S (T)");
                $x= -19;
                for ($i=0; $i <= 38 ; $i++) { 
                    $sheet->setCellValue("AZ".($i+3),$x);
                    $x++;
                }

                $sheet->setCellValue("BA1","%");
                $sheet->setCellValue("BA3",'1');
                $sheet->setCellValue("BA4",'2');
                $sheet->setCellValue("BA5",'2');
                $sheet->setCellValue("BA6",'2');
                $sheet->setCellValue("BA7",'2');
                $sheet->setCellValue("BA8",'2');
                $sheet->setCellValue("BA9",'2');
                $sheet->setCellValue("BA10",'3');
                $sheet->setCellValue("BA11",'4');
                $sheet->setCellValue("BA12",'5');
                $sheet->setCellValue("BA13",'8');
                $sheet->setCellValue("BA14",'10');
                $sheet->setCellValue("BA15",'15');
                $sheet->setCellValue("BA16",'20');
                $sheet->setCellValue("BA17",'25');
                $sheet->setCellValue("BA18",'30');
                $sheet->setCellValue("BA19",'35');
                $sheet->setCellValue("BA20",'40');
                $sheet->setCellValue("BA21",'50');
                $sheet->setCellValue("BA22",'57');
                $sheet->setCellValue("BA23",'60');
                $sheet->setCellValue("BA24",'70');
                $sheet->setCellValue("BA25",'75');
                $sheet->setCellValue("BA26",'80');
                $sheet->setCellValue("BA27",'84');
                $sheet->setCellValue("BA28",'87');
                $sheet->setCellValue("BA29",'91');
                $sheet->setCellValue("BA30",'94');
                $sheet->setCellValue("BA31",'96');
                $sheet->setCellValue("BA32",'97');
                $sheet->setCellValue("BA33",'97');
                $sheet->setCellValue("BA34",'98');
                $sheet->setCellValue("BA35",'98');
                $sheet->setCellValue("BA36",'98');
                $sheet->setCellValue("BA37",'98');
                $sheet->setCellValue("BA38",'98');
                $sheet->setCellValue("BA39",'98');
                $sheet->setCellValue("BA40",'98');
                $sheet->setCellValue("BA41",'99');

                $sheet->setCellValue("BC1","C (T)");
                $x= -16;
                for ($i=0; $i <= 31 ; $i++) { 
                    $sheet->setCellValue("BC".($i+3),$x);
                    $x++;
                }

                $sheet->setCellValue("BD1","%");
                $sheet->setCellValue("BD3",'1');
                $sheet->setCellValue("BD4",'2');
                $sheet->setCellValue("BD5",'2');
                $sheet->setCellValue("BD6",'2');
                $sheet->setCellValue("BD7",'2');
                $sheet->setCellValue("BD8",'3');
                $sheet->setCellValue("BD9",'4');
                $sheet->setCellValue("BD10",'6');
                $sheet->setCellValue("BD11",'9');
                $sheet->setCellValue("BD12",'13');
                $sheet->setCellValue("BD13",'20');
                $sheet->setCellValue("BD14",'25');
                $sheet->setCellValue("BD15",'35');
                $sheet->setCellValue("BD16",'40');
                $sheet->setCellValue("BD17",'55');
                $sheet->setCellValue("BD18",'60');
                $sheet->setCellValue("BD19",'70');
                $sheet->setCellValue("BD20",'75');
                $sheet->setCellValue("BD21",'84');
                $sheet->setCellValue("BD22",'90');
                $sheet->setCellValue("BD23",'95');
                $sheet->setCellValue("BD24",'96');
                $sheet->setCellValue("BD25",'97');
                $sheet->setCellValue("BD26",'97');
                $sheet->setCellValue("BD27",'98');
                $sheet->setCellValue("BD28",'98');
                $sheet->setCellValue("BD29",'98');
                $sheet->setCellValue("BD30",'98');
                $sheet->setCellValue("BD31",'98');
                $sheet->setCellValue("BD32",'98');
                $sheet->setCellValue("BD33",'98');
                $sheet->setCellValue("BD34",'99');


                $sheet->getColumnDimension('V')->setVisible(false);
                $sheet->getColumnDimension('W')->setVisible(false);
                $sheet->getColumnDimension('X')->setVisible(false);
                $sheet->getColumnDimension('Y')->setVisible(false);
                $sheet->getColumnDimension('Z')->setVisible(false);
                $sheet->getColumnDimension('AA')->setVisible(false);
                $sheet->getColumnDimension('AB')->setVisible(false);
                $sheet->getColumnDimension('AC')->setVisible(false);
                $sheet->getColumnDimension('AD')->setVisible(false);
                $sheet->getColumnDimension('AE')->setVisible(false);
                $sheet->getColumnDimension('AF')->setVisible(false);
                $sheet->getColumnDimension('AG')->setVisible(false);
                $sheet->getColumnDimension('AH')->setVisible(false);
                $sheet->getColumnDimension('AI')->setVisible(false);
                $sheet->getColumnDimension('AJ')->setVisible(false);
                $sheet->getColumnDimension('AK')->setVisible(false);
                $sheet->getColumnDimension('AL')->setVisible(false);
                $sheet->getColumnDimension('AM')->setVisible(false);
                $sheet->getColumnDimension('AN')->setVisible(false);
                $sheet->getColumnDimension('AO')->setVisible(false);
                $sheet->getColumnDimension('AP')->setVisible(false);
                $sheet->getColumnDimension('AQ')->setVisible(false);
                $sheet->getColumnDimension('AR')->setVisible(false);
                $sheet->getColumnDimension('AS')->setVisible(false);
                $sheet->getColumnDimension('AT')->setVisible(false);
                $sheet->getColumnDimension('AU')->setVisible(false);
                $sheet->getColumnDimension('AV')->setVisible(false);
                $sheet->getColumnDimension('AW')->setVisible(false);
                $sheet->getColumnDimension('AX')->setVisible(false);
                $sheet->getColumnDimension('AY')->setVisible(false);
                $sheet->getColumnDimension('AZ')->setVisible(false);
                $sheet->getColumnDimension('BA')->setVisible(false);
                $sheet->getColumnDimension('BB')->setVisible(false);
                $sheet->getColumnDimension('BC')->setVisible(false);
                $sheet->getColumnDimension('BD')->setVisible(false);

                $sheet->setCellValue("Q13","BUSCARV(Q7;V2:W23;2)");
                $sheet->setCellValue("Q14","BUSCARV(Q8;AH3:AI24;2)");
                $sheet->setCellValue("Q15","BUSCARV(Q9;AT3:AU44;2)");

                $sheet->setCellValue("R13","BUSCARV(R7;Y3:Z20;2)");
                $sheet->setCellValue("R14","BUSCARV(R8;AK3:AL22;2)");
                $sheet->setCellValue("R15","BUSCARV(R9;AW3:AX39;2)");

                $sheet->setCellValue("S13","BUSCARV(S7;AB3:AC22;2)");
                $sheet->setCellValue("S14","BUSCARV(S8;AN3:AO22;2)");
                $sheet->setCellValue("S15","BUSCARV(S9;AZ3:BA41;2)");

                $sheet->setCellValue("T13","BUSCARV(T7;AE3:AF18;2)");
                $sheet->setCellValue("T14","BUSCARV(T8;AQ3:AR19;2)");
                $sheet->setCellValue("T15","BUSCARV(T9;BC3:BD34;2)");

                $sheet->setCellValue("U13","Presion");
                $sheet->setCellValue("U14","Motivacion");
                $sheet->setCellValue("U15","Normal");

             });
        })->export('xlsx');
        return view('usuarios.create');
    }
}