<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* IPV */
Route::resource('/ipv', 'UsuariosController');
Route::post('ipv/store', 'UsuariosController@store');
Route::get('ipv/excel/{id}', 'UsuariosController@excel');

/* Cleaver */
Route::resource('/cleaver', 'CleaverController');
Route::post('cleaver/store', 'CleaverController@store');
Route::get('cleaver/excel/{id}', 'CleaverController@excel');