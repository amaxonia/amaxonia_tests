<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuestas extends Model
{
    protected $table = 'respuestas';
    public  $timestamps = false;
    protected $fillable = ['test','resp1','resp2','resp3','resp4','resp5','resp6','resp7','resp8','resp9','resp10','resp11','resp12','resp13','resp14','resp15','resp16','resp17','resp18','resp19','resp20','resp21','resp22','resp23','resp24','resp25','resp26','resp27','resp28','resp29','resp30','resp31','resp32','resp33','resp34','resp35','resp36','resp37','resp38','resp39','resp40','resp41','resp42','resp43','resp44','resp45','resp46','resp47','resp48','resp49','resp50','resp51','resp52','resp53','resp54','resp55','resp56','resp57','resp58','resp59','resp60','resp61','resp62','resp63','resp64','resp65','resp66','resp67','resp68','resp69','resp70','resp71','resp72','resp73','resp74','resp75','resp76','resp77','resp78','resp79','resp80','resp81','resp82','resp83','resp84','resp85','resp86','resp87','resp88','resp89','resp90','resp91','resp92','resp93','resp94','resp95','resp96','resp97','resp98','resp99','resp100'];
}