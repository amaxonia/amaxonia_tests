<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
		<title>@yield('title') | Amaxonia ERP www.amaxoniaerp.com</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		<link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&display=swap" rel="stylesheet">

		<link href="{{ asset('css') }}/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet">
		<link href="{{ asset('css') }}/bootstrap/bootstrap.min.css" rel="stylesheet">
		<link href="{{ asset('css') }}/base.css" rel="stylesheet">
		<link href="{{ asset('css') }}/style.css" rel="stylesheet">
		<style type="text/css">
			body{
				font-family: 'PT Sans Narrow', sans-serif;
			}
		</style>
		@yield('css')
	</head>

	<body>
 		<div class="container-fluid">
			@yield('content')
		</div>

		<script src="{{ asset('js') }}/jquery-2.2.4.min.js"></script>
		<script src="{{ asset('js') }}/bootstrap/bootstrap.min.js"></script>
        <script src="{{ asset('js') }}/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="{{ asset('js') }}/bootstrap-sweetalert/jquery.sweet-alert.init.js"></script>
		@yield('js')
		<script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
	</body>
</html>