@extends('layout')

@section('title', 'Test Cleaver')

@section('css')
    <style>
		select.form-control {
			background-color: #fff !important;
		}
		select.form-control {
			border: 1px solid #b7b7b7 !important;
			width: 100% !important;
		}
		.radio {
			display: block;
			position: relative;
			padding-left: 30px;
			margin-bottom: 12px;
			cursor: pointer;
			font-size: 20px;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none
		}
		.radio input {
			position: absolute;
			opacity: 0;
			cursor: pointer;
		}
		.checkround {
			position: absolute;
			/*top: 6px;*/
			left: 0;
			height: 20px;
			width: 20px;
			background-color: #fff ;
			border-color:#00bcd4;
			border-style:solid;
			border-width:2px;
			border-radius: 50%;
		}
		.radio input:checked ~ .checkround {
			background-color: #fff;
		}
		.checkround:after {
			content: "";
			position: absolute;
			display: none;
		}
		.radio input:checked ~ .checkround:after {
			display: block;
		}
		.radio .checkround:after {
			left: 2px;
			top: 2px;
			width: 12px;
			height: 12px;
			border-radius: 50%;
			background:#00bcd4;
		}
		.radio, .checkbox {
			display: block;
			min-height: 0px;
			margin-top: 0px;
			margin-bottom: 0px;
			padding-left: 0px;
		}
		.table-borderless td, .table-borderless th {
			border: none !important;
		}
    </style>
@endsection

@section('content')
	<div class="row">
		<section class="content">
			<div class="col-md-12">
				<form id="formRegistro">
                	<img src="{{ url('img') }}/logo.png" alt="">					
					<div id="parte_0">
						<section class="intro first" style="font-size: 19px">
							<p>Bienvenido,</p>
							<p>por favor, dedique unos minutos de su tiempo para rellenar el siguiente cuestionario.</p>
						</section>

						<fieldset class="required">
							<h1>
								<div class="title-part text-center font-weight-bold">
									TEST CLEAVER
								</div>
							</h1>
							<h2>
								<div class="title-part">
									Por favor, rellena el siguiente formulario con tus datos personales
								</div>
							</h2>
							<div class="special-padding-row">
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4">
										<div class="form-group">
											<label class="font-weight-bold text-left">Nombre</label>
											<input type="text" name="nombre" id="nombre" class="form-control input-sm" placeholder="Nombre">
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<div class="form-group">
											<label class="font-weight-bold text-left">Estado Civil</label>
											<select name="estado" id="estado" class="form-control input-sm">
												<option selected disabled>Estado Civil</option>
												<option value="Soltero/a">Soltero/a</option>
												<option value="Casado/a">Casado/a</option>
												<option value="Divorciado/a">Divorciado/a</option>
												<option value="Viudo/a">Viudo/a</option>
											</select>
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<div class="form-group">
											<label class="font-weight-bold text-left">Sexo</label>
											<select name="sexo" id="sexo" class="form-control input-sm">
												<option selected disabled>Sexo</option>
												<option value="F">Femenino</option>
												<option value="M">Masculino</option>
											</select>
										</div>
									</div>

									<div class="col-xs-4 col-sm-4 col-md-4">
										<div class="form-group">
											<label class="font-weight-bold text-left">Fecha Nacimiento</label>
											<input type="date" name="nacimiento" id="nacimiento" class="form-control input-sm" placeholder="Fecha Nacimiento">
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<div class="form-group">
											<label class="font-weight-bold text-left">Escolaridad</label>
											<input type="text" name="escolaridad" id="escolaridad" class="form-control input-sm" placeholder="Escolaridad">
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<div class="form-group">
											<label class="font-weight-bold text-left">Puesto</label>
											<input type="text" name="puesto" id="puesto" class="form-control input-sm" placeholder="Puesto">
										</div>
									</div>
								</div>
							</div>
						</fieldset>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
								<a href="#" class="btn btn-success btn-block" id="btn_0" disabled>Iniciar Prueba</a>
								<!--input type="submit" value="Finalizar Prueba" class="btn btn-success btn-block" id="btn_9" disabled-->
							</div>
						</div>
					</div>

					<div id="parte_1">						
						<fieldset class="required" style="background: #ececec;">
							<h1>
								<div class="title-part text-center font-weight-bold">
									Test Cleaver
								</div>
							</h1>

							<div class="alert alert-amaxonia mx-0 my-0 font-weight-1 fs-16">
								INSTRUCCIONES: Las palabras descriptivas que verá a continuación se encuentran agrupadas en series de cuatro, examine las palabras de cada serie y seleccione la opción bajo la columna "MÁS" de la palabra que mejor describa su forma de ser o de comportarse. Después seleccione la opción en la palabra que menos lo describa o se acerque a su forma de ser, bajo la columna de "MENOS".
							</div>

							<fieldset class="required pb-2">
								<table class="table table-header-rotated table-condensed table-borderless mb-0">
								  	<thead>
									    <tr>
											<th></th>
											<th class="rotate"><div><span>MÁS</span></div></th>
											<th class="rotate"><div><span>MENOS</span></div></th>
											<th></th>
											<th class="rotate"><div><span>MÁS</span></div></th>
											<th class="rotate"><div><span>MENOS</span></div></th>
											<th></th>
											<th class="rotate"><div><span>MÁS</span></div></th>
											<th class="rotate"><div><span>MENOS</span></div></th>
											<th></th>
											<th class="rotate"><div><span>MÁS</span></div></th>
											<th class="rotate"><div><span>MENOS</span></div></th>
									    </tr> 
								  	</thead>
								  	<tbody>
								    	<tr>
								      		<th class="row-header">Persuasivo</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp1" name="resp1" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp1" name="resp1" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Fuerza de Voluntad</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp2" name="resp2" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp2" name="resp2" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Obediente</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp3" name="resp3" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp3" name="resp3" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Aventurero</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp4" name="resp4" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp4" name="resp4" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Gentil</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp5" name="resp5" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp5" name="resp5" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Mente Abierta</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp6" name="resp6" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp6" name="resp6" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Quisquilloso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp7" name="resp7" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp7" name="resp7" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Receptivo</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp8" name="resp8" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp8" name="resp8" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Humilde</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp9" name="resp9" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp9" name="resp9" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Complaciente</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp10" name="resp10" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp10" name="resp10" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Inconquistable</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp11" name="resp11" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp11" name="resp11" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Cordial</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp12" name="resp12" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp12" name="resp12" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Original</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp13" name="resp13" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp13" name="resp13" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Animoso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp14" name="resp14" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp14" name="resp14" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Juguetón</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp15" name="resp15" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp15" name="resp15" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Moderado</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp16" name="resp16" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp16" name="resp16" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr><td></td></tr>
								    	<tr>
								      		<th class="row-header">Agresivo</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp17" name="resp17" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp17" name="resp17" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Confiado</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp18" name="resp18" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp18" name="resp18" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Respetuoso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp19" name="resp19" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp19" name="resp19" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Indulgente</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp20" name="resp20" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp20" name="resp20" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Alma de la Fiesta</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp21" name="resp21" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp21" name="resp21" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Simpatizador</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp22" name="resp22" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp22" name="resp22" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Emprendedor</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp23" name="resp23" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp23" name="resp23" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Esteta</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp24" name="resp24" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp24" name="resp24" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Comodino</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp25" name="resp25" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp25" name="resp25" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Tolerante</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp26" name="resp26" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp26" name="resp26" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Optimista</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp27" name="resp27" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp27" name="resp27" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Vigoroso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp28" name="resp28" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp28" name="resp28" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Temeroso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp29" name="resp29" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp29" name="resp29" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Afirmativo</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp30" name="resp30" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp30" name="resp30" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Servicial</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp31" name="resp31" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp31" name="resp31" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Sociable</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp32" name="resp32" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp32" name="resp32" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr><td></td></tr>
								    	<tr>
								      		<th class="row-header">Agradable</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp33" name="resp33" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp33" name="resp33" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Ecuánime</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp34" name="resp34" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp34" name="resp34" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Valiente</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp35" name="resp35" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp35" name="resp35" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Parlanchín</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp36" name="resp36" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp36" name="resp36" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Temeroso de Dios</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp37" name="resp37" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp37" name="resp37" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Preciso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp38" name="resp38" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp38" name="resp38" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Inspirador</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp39" name="resp39" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp39" name="resp39" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Controlado</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp40" name="resp40" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp40" name="resp40" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Tenaz</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp41" name="resp41" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp41" name="resp41" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Nervioso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp42" name="resp42" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp42" name="resp42" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Sumiso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp43" name="resp43" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp43" name="resp43" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Convencional</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp44" name="resp44" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp44" name="resp44" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Atractivo</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp45" name="resp45" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp45" name="resp45" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Jovial</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp46" name="resp46" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp46" name="resp46" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Tímido</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp47" name="resp47" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp47" name="resp47" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Decisivo</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp48" name="resp48" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp48" name="resp48" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr><td></td></tr>
								    	<tr>
								      		<th class="row-header">Cauteloso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp49" name="resp49" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp49" name="resp49" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Disciplinado</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp50" name="resp50" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp50" name="resp50" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Adaptable</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp51" name="resp51" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp51" name="resp51" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Cohibido</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp52" name="resp52" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp52" name="resp52" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Determinado</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp53" name="resp53" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp53" name="resp53" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Generoso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp54" name="resp54" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp54" name="resp54" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Disputador</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp55" name="resp55" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp55" name="resp55" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Exacto</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp56" name="resp56" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp56" name="resp56" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Convincente</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp57" name="resp57" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp57" name="resp57" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Animoso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp58" name="resp58" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp58" name="resp58" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Indiferente</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp59" name="resp59" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp59" name="resp59" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Franco</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp60" name="resp60" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp60" name="resp60" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Bonachón</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp61" name="resp61" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp61" name="resp61" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Persistente</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp62" name="resp62" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp62" name="resp62" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Sangre Liviana</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp63" name="resp63" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp63" name="resp63" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Buen Compañero</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp64" name="resp64" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp64" name="resp64" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr><td></td></tr>
								    	<tr>
								      		<th class="row-header">Dócil</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp65" name="resp65" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp65" name="resp65" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Competitivo</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp66" name="resp66" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp66" name="resp66" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Amiguero</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp67" name="resp67" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp67" name="resp67" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Diplomático</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp68" name="resp68" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp68" name="resp68" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Atrevido</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp69" name="resp69" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp69" name="resp69" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Alegre</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp70" name="resp70" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp70" name="resp70" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Paciente</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp71" name="resp71" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp71" name="resp71" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Audaz</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp72" name="resp72" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp72" name="resp72" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Leal</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp73" name="resp73" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp73" name="resp73" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Considerado</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp74" name="resp74" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp74" name="resp74" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Confianza en si Mismo</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp75" name="resp75" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp75" name="resp75" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Refinado</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp76" name="resp76" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp76" name="resp76" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Encantador</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp77" name="resp77" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp77" name="resp77" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Armonioso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp78" name="resp78" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp78" name="resp78" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Mesurado para Hablar</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp79" name="resp79" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp79" name="resp79" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Satisfecho</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp80" name="resp80" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp80" name="resp80" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr><td></td></tr>
								    	<tr>
								      		<th class="row-header">Dispuesto</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp81" name="resp81" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp81" name="resp81" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Admirable</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp82" name="resp82" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp82" name="resp82" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Conforme</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp83" name="resp83" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp83" name="resp83" value="0">
												<span class="checkround"></span>
												</label>
											</td>
											<th class="row-header">Inquieto</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp84" name="resp84" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp84" name="resp84" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Deseoso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp85" name="resp85" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp85" name="resp85" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Bondadoso</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp86" name="resp86" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp86" name="resp86" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Confiable</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp87" name="resp87" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp87" name="resp87" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Popular</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp88" name="resp88" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp88" name="resp88" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Consecuente</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp89" name="resp89" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp89" name="resp89" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Resignado</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp90" name="resp90" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp90" name="resp90" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Pacífico</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp91" name="resp91" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp91" name="resp91" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Buen Vecino</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="res92" name="res92" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="res92" name="res92" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								    	<tr>
								      		<th class="row-header">Entusiasta</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp93" name="resp93" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp93" name="resp93" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Carácter Firme</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp94" name="resp94" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp94" name="resp94" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Positivo</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp95" name="resp95" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp95" name="resp95" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								      		<th class="row-header">Devoto</th>
								      		<td>
								  				<label class="radio">
												<input type="radio" id="resp96" name="resp96" value="1">
												<span class="checkround"></span>
												</label>
											</td>
											<td>
								  				<label class="radio">
												<input type="radio" id="resp96" name="resp96" value="0">
												<span class="checkround"></span>
												</label>
											</td>
								    	</tr>
								  	</tbody>
								</table>
							</fieldset>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
									<input type="submit" value="Finalizar Prueba" class="btn btn-success btn-block">
								</div>
							</div>
						</fieldset>
					</div>
				</form>
			</div>
		</section>
	</div>
@endsection

@section('js')
	<script src="{{ asset('js') }}/cleaver.js"></script>
@endsection