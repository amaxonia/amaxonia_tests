@extends('layout')

@section('title', 'IPV')

@section('css')
    
@endsection

@section('content')
	<div class="row">
		<section class="content">
			<div class="col-md-12">
				<form id="formRegistro">
                <img src="{{ url('img') }}/logo.png" alt="">					
					<div id="parte_0">
						<section class="intro first" style="font-size: 19px">
							<p>Bienvenido,</p>
							<p>por favor, dedique unos minutos de su tiempo para rellenar el siguiente cuestionario.</p>
						</section>

						<fieldset class="required">
							<h1>
								<div class="title-part text-center font-weight-bold">
									TEST IPV
								</div>
							</h1>
							<h2>
								<div class="title-part">
									Por favor, rellena el nombre y edad:
								</div>
							</h2>
							<div class="special-padding-row">
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
											<input type="text" name="nombre" id="nombre" class="form-control input-sm" placeholder="Nombre">
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group">
											<input type="number" name="edad" id="edad" class="form-control input-sm" placeholder="edad">
										</div>
									</div>
								</div>
							</div>
							<div class="alert alert-amaxonia mx-4 my-0 font-weight-1 fs-16">
								INSTRUCCIONES: Escoja una alternativa, una sola, para cada pregunta, la que de manera espontánea le parezca preferible. No hay respuestas buenas ni malas, cada uno piensa y actua como cree conveniente en función de su crácter, de sus interes, etc.
							</div>
						</fieldset>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
								<a href="#" class="btn btn-success btn-block" id="btn_0" disabled>Iniciar Prueba</a>
							</div>
						</div>
					</div>

					<div id="parte_1">						
						<fieldset class="required" style="background: #ececec;">
							<h1>
								<div class="title-part text-center">
									SECCIÓN 1
								</div>
							</h1>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">1</span>
										T... debe salir de viaje con una persona de la que no conoce nada. ¿Sobre cuál de los siguientes aspectos de esta persona es preferible informar a T para que el viaje resulte mejor?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp1" id="resp1" value="1" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Su estilo de vida</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp1" id="resp1" value="2" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Los puntos que tengan en común</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp1" id="resp1" value="3" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Su actividad y responsabilidades profesionales</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">2</span>
										Entre los siguientes tipos de vendedores de prendas confeccionadas, ¿Cuál es el que tiene màs probabilidades de èxito?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp2" id="resp2" value="1" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. El que presente las últimas novedades</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp2" id="resp2" value="2" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. El que, tratando de conocer el estilo de su cliente, se interese por su modo de vida</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp2" id="resp2" value="3" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. El que posea una buena capacidad de convencer</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">3</span>
										B... tiene un proyecto importante para la promociòn de un nuevo producto y va a exponer su idea ante el Comité de Dirección ¿Cuàl de las siguientes cualidades le será más útil para persuadir a su auditorio?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp3" id="resp3" value="1" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Competencia técnica y un conocimiento perfecto del tema</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp3" id="resp3" value="2" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Capacidad para modificar sus razonamientos según la actitud del auditorio</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp3" id="resp3" value="3" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Facultad para mantener el orden de sus ideas a pesar de las interrupciones</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">4</span>
										Se envía a G..., contra su voluntad, a un país extranjero, por el cual no se siente atraído en principio, para una estancia de varias semanas ¿Cuál será su actitud?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp4" id="resp4" value="1" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Estimar que la duración de su estancia es demasiado corta para conseguir integrarse</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp4" id="resp4" value="2" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Tratar de aprender la lengua para comprender mejor a este país y vivir más a gusto en él</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp4" id="resp4" value="3" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. No tener más que los contactos estrictamente necesarios para la buena marcha de su trabajo</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">5</span>
										Según su opinión, las personas que dicen siempre "su" verdad a los demás, aunqué ésta sea desagradable, lo hacen, en general, porque:
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp5" id="resp5" value="1" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. No saben controlar sus impulsos y dicen espontáneamente lo que piensan</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp5" id="resp5" value="2" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. No les gusta la hipocresía</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp5" id="resp5" value="3" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Piensan que esto simplifica las relaciones</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">6</span>
										Un buen amigo de C... había comenzado bastante brillantemente su carrera profesional, pero los resultados no han sido los que cabría esperar y terminó teniendo numerosos fracasos. ¿Qué piensa C... acerca de ésto?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp6" id="resp6" value="1" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Que las condiciones no le han sido favorables</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp6" id="resp6" value="2" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Que no estaba a la altura necesaria</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp6" id="resp6" value="3" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Que no ha utilizado bien los medios para salir adelante</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">7</span>
										B... encuentra, en casa de unos amigos, a una persona que aparenta una edad muy inferior a la que realmente tiene. ¿Qué opinará B...?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp7" id="resp7" value="1" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Probablemente ha encontrado en la vida lo que le convenía</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp7" id="resp7" value="2" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Tiene buena suerte; es cuestión de naturaleza</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp7" id="resp7" value="3" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Seguramente ha tenido una vida facil</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">8</span>
										Para expansionarse, S... decide aprender judo. Después de unos meses de entrenamiento se da cuenta de que progresa muy lentamente. ¿Cuál sería su reacción?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp8" id="resp8" value="1" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Pensando que realmente no está hecho para el judo, elegirá otro deporte</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp8" id="resp8" value="2" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Convencido de que no está dotado para las actividades corporales buscará otra forma de actividad</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp8" id="resp8" value="3" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Continuará sus esfuerzos con la esperanza de que seguramente serán un día coronados por el éxito</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">9</span>
										P... se acuesta una noche muy fatigado, pero no puede dormirse porque sus vecinos del piso superior han organizado una fiesta muy ruidosa. ¿Qué hará?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp9" id="resp9" value="1" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Subir y advertir a sus vecinos</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp9" id="resp9" value="2" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Dar con la escoba algunos golpes en el techo</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp9" id="resp9" value="3" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Tomar un somnífero y tratar de dormir cueste lo que cueste</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">10</span>
										G..., que carece de teléfono, llega a una oficina de telégrafos para enviar un telegrama a la hora de cerrar. El encargado le dice que es demasiado tarde, que va a cerrar. ¿Qué hará G.?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp10" id="resp10" value="1" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Ir a casa de un amigo que tiene teléfono para poner el telegrama por teléfono</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp10" id="resp10" value="2" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Convencer al encargado de que su telegrama es muy urgente y que debe salir inmediatamente</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp10" id="resp10" value="3" onclick="checkRespuesta(1);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Ir a una oficina de telégrafos que cierra más tarde</span>
										</label>
									</div>
								</div>
							</fieldset>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
									<a href="#" class="btn btn-success btn-block" id="btn_1" disabled>Siguiente Sección</a>
								</div>
							</div>
						</fieldset>
					</div>

					<div id="parte_2">
						<fieldset class="required" style="background: #ececec;">
							<h1>
								<div class="title-part text-center">
									SECCIÓN 2
								</div>
							</h1>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">11</span>
										X... hace un viaje de turismo por un país en que es costumbre discutir los precios ¿ Cuál será su actitud?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp11" id="resp11" value="1" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Hará pocas compras porque le molesta tener que discutir siempre</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp11" id="resp11" value="2" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Hará numerosas compras, incluso un poco inútiles, porque le encanta discutir los precios</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp11" id="resp11" value="3" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Convencido de que le timarían, preferirá los almacenes de precios fijos</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">12</span>
										Sin tener en cuenta la formación y competencia necesarias, ¿Cuál de las siguientes direcciones de servicios le gustaría asumir?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp12" id="resp12" value="1" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Gestión</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp12" id="resp12" value="2" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Personal</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp12" id="resp12" value="3" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Publicidad</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">13</span>
										Un escritor poco conocido acaba de recibir el premio Planeta. ¿Qué cree usted que prefiera hacer?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp13" id="resp13" value="1" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Escribir menos y aprovecharse de su éxito para salir, recibir a la gente y hacer una vida de sociedad</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp13" id="resp13" value="2" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Intentar escribir una obra maestra</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp13" id="resp13" value="3" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Dedicarse a la pintura e intentar triunfar también en ella</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">14</span>
										Si usted se encontrara en una situación de examen en la que pudiera elegir entre dos temas, ¿Cuál escogería?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp14" id="resp14" value="1" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Un tema ampliamente tratado durante el curso con el que tiene, casi, la plena seguridad de alcanzar la nota media necesaria</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp14" id="resp14" value="2" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Un tema que le permita, con un mínimo de conocimientos, pero mucha lógica e imaginación, obtener una calificación muy buena pero, quizá, también muy mala</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">15</span>
										Entre las siguientes actividades de descanso físico en la naturaleza, ¿Cuál prefiere usted?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp15" id="resp15" value="1" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Una vuelta por la bahía cuando el mar está agitado</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp15" id="resp15" value="2" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Un baño de sol en una playa de arena fina, en un hermoso día de verano con el aire en calma</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp15" id="resp15" value="3" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Un paseo por el campo en primavera</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">16</span>
										¿Qué es, a su juicio, lo que atrae a más gente en los viajes o estancias en el extranjero?

									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp16" id="resp16" value="1" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Los paisajes nuevos</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp16" id="resp16" value="2" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. El descubrimiento de otra civilización artística</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp16" id="resp16" value="3" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. El contacto con una población de costumbres muy diferentes</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">17</span>
										P... camina rápidamente por la calle y parece que tiene prisa. Un joven que realiza una encuesta le detiene para hacerle algunas preguntas extrañas, pero diverditas, ¿Qué hará P...?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp17" id="resp17" value="1" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Rehusar con firmeza contestar a las preguntas</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp17" id="resp17" value="2" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Responder rápidamente, porque lo encuentra divertido</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp17" id="resp17" value="3" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Excusarse de no tener tiempo para responder, sintiendo de verdad no poder hacerlo</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">18</span>
										L... trabaja en una empresa en que recibe muy pocas informaciones a nivel oficial ¿Qué opina usted de ello?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp18" id="resp18" value="1" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Qué será muy dificil obtener información</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp18" id="resp18" value="2" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Que con los "rumores de pasillo", esto no constituye un problema</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp18" id="resp18" value="3" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Que, de hecho, es muy fácil estar informado; bastará conocer a algunas personas bien situadas</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">19</span>
										Son las 11 la noche. Hay muchos vehículos aparcados en la calle, algunos de ellos sobre las aceras. P... ve desde su ventana a uno que trata de abrir la puerta de un coche; no tiene llave y utiliza un destornillador ¿Cuál será la reacción de P...?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp19" id="resp19" value="1" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Probablemente es un ratero que trata de robar un coche. ¿Y si yo llamase a la policía?</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp19" id="resp19" value="2" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Alguien ha quedado atrapado en la acera y trata de mover un coche para salir</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp19" id="resp19" value="3" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. ¿Qué estará haciendo? Parece que utiliza un destornillador</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">20</span>
										Cuando un niño ha cometido una gran tontería, ¿Cuál es, a su modo de ver, la mejor reacción?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp20" id="resp20" value="1" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Castigarle, explicándole por qué se hace</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp20" id="resp20" value="2" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Explicarle la significación de su tontería, situándola en su contexto</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp20" id="resp20" value="3" onclick="checkRespuesta(2);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Apelar al amor propio del niño</span>
										</label>
									</div>
								</div>
							</fieldset>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
									<a href="#" class="btn btn-success btn-block" id="btn_2" disabled>Siguiente Sección</a>
								</div>
							</div>
						</fieldset>
					</div>

					<div id="parte_3">
						<fieldset class="required" style="background: #ececec;">
							<h1>
								<div class="title-part text-center">
									SECCIÓN 3
								</div>
							</h1>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">21</span>
										B... tiene varios hijos y ha decidido intervernir en la educación sexual de cada uno de ellos. ¿Qué procedimiento le parece a usted que adoptará probablemente?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp21" id="resp21" value="1" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Reflexionar sobre el asunto y preparar, con el hijo mayor un tipo de intervención que luego mantendrá a los demás hijos</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp21" id="resp21" value="2" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Seguir su intuición o sus sentimientos</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp21" id="resp21" value="3" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Adoptar un modo de proceder distinto para cada hijo</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">22</span>
										¿Cuál es para un niño la manera más "astuta" de presentar unas calificaciones escolares poco brillantes?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp22" id="resp22" value="1" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Enseñarlas lo antes posible para librarse de ello</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp22" id="resp22" value="2" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Presentarlas el Lunes a la hora del desayuno</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp22" id="resp22" value="3" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Presentarlas en el momento en que pueda aportar, además, alguna compensación</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">23</span>
										El hijo de G... ha entrado en el despacho de su padre a pesar de que éste se lo prohibió, y ha derramado un frasco de tinta sobre papeles importantes. G... está furioso, le da un buen tortazo ¿Qué pensará G... algún tiempo después?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp23" id="resp23" value="1" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Que un buen azote de vez en cuando no viene mal</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp23" id="resp23" value="2" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Que tal vez se ha excedido un poco</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp23" id="resp23" value="3" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Que es conveniente enfadarse de verdad una vez para no tener que volver a hacerlo</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">24</span>
										Una persona con la que D... se relaciona muy frecuentemente acaba de jugarle una mala pasada ¿Cuál será la reacción de D...?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp24" id="resp24" value="1" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. No muestra ninguna, pero se da cuenta de que le será muy difícil ocultar durante mucho tiempo su resentimiento</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp24" id="resp24" value="2" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. No dice nada porque piensa que el daño causado es tal vez menor del que había creído</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp24" id="resp24" value="3" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Se esfuerza en ocultar su resentimiento para no entorpecer sus futuras relaciones</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">25</span>
										N... asiste a una comida. Se da cuenta de que, a causa de una opinión que se le ha escapado, acaba de provocar una situación tensa en el grupo. ¿Qué le parece a usted que será para él lo más desagradable?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp25" id="resp25" value="1" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. El sentimiento de que algo va a dificultar notablemente el contacto</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp25" id="resp25" value="2" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. El pensamiento de que se le va a juzgar a causa de esta observación</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp25" id="resp25" value="3" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. El disgusto de haber hablado sin pensar</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">26</span>
										P... está jugando a las cartas con sus amigos; a pesar de sus esfuerzos, pierde varias veces seguidas. ¿Cuál es, según usted, su reacción más probable?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp26" id="resp26" value="1" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Después de todo, esta tarde he aprendido bastante. Seguramente ganaré la próxima vez</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp26" id="resp26" value="2" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Siempre ocurre lo mismo; el juego de las cartas no se me da bien</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp26" id="resp26" value="3" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Realmente no estoy en forma esta tarde</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">27</span>
										R... está sentado en un cine al lado de personas que no se callan y hacen frecuentes comentarios ¿Qué hará R...?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp27" id="resp27" value="1" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Rogarles que se callen</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp27" id="resp27" value="2" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Cambiar de lugar</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp27" id="resp27" value="3" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Concentrarse más en la película para evitar la molestia</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">28</span>
										T. trabaja en una gran empresa. Ha efectuado algunos cambios con la organización del trabajo y es muy critidaco por gran parte de sus colegas. ¿Cuál será la reacción de T...?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp28" id="resp28" value="1" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Tratar de justificar su posición ante sus colegas</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp28" id="resp28" value="2" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Pensar que, en cualquier caso, nunca se consigue la unanimidad</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp28" id="resp28" value="3" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Replantearse el problema</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">29</span>
										¿Qué piensa usted cuando ve a un niño enfrentarse a los adultos?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp29" id="resp29" value="1" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Está mal educado</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp29" id="resp29" value="2" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Probablemente tiene serias dificultades en sus relaciones con los demás</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp29" id="resp29" value="3" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Posee una fuerte personalidad</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">30</span>
										M... debe contratar a un colaborador que se encargará de la dirección de un servicio. Teniendo en cuenta que tendrá responsabilidades jerárquicas, ¿a cuàl de las cualidades siguientes dará M... más importancia?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp30" id="resp30" value="1" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Tener autoridad</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp30" id="resp30" value="2" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Ser convincente</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp30" id="resp30" value="3" onclick="checkRespuesta(3);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Ser justo</span>
										</label>
									</div>
								</div>
							</fieldset>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
									<a href="#" class="btn btn-success btn-block" id="btn_3" disabled>Siguiente Sección</a>		
								</div>
							</div>
						</fieldset>
					</div>

					<div id="parte_4">
						<fieldset class="required" style="background: #ececec;">
							<h1>
								<div class="title-part text-center">
									SECCIÓN 4
								</div>
							</h1>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">31</span>
										Entre los siguientes estilos de conducir un automovil en carretera, ¿cuàl es, a su parecer, el más frecuentemente adoptado por los automovilistas?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp31" id="resp31" value="1" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Pensando en la mecánica y en la seguridad, apenas fuerzan la velocidad de su vehículo</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp31" id="resp31" value="2" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Estimando que es tan peligroso circular demasiado despacio como demasiado deprisa, adelantan a los vehículos que les obligan a ir lentamente</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp31" id="resp31" value="3" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Toleran de mala gana no tener vía libre y adelantan sistemáticamente a los vehículos que van delante de ellos</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">32</span>
										¿Qué es lo que màs valora cuando usted va a un restaurante?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp32" id="resp32" value="1" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Una buena cocina</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp32" id="resp32" value="2" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Un ambiente agradable</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp32" id="resp32" value="3" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. La posibilidad de comer el plato especial de la casa</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">33</span>
										Una sra. va de compras a un almacén que se encuentra muy lejos de su casa. A la puerta del almacén se da cuenta de que ha olvidado su monedero y su talonario de cheques. Sin tener en cuenta el tiempo de que dispone, ¿Qué piensa usted que hará?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp33" id="resp33" value="1" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Volver a su casa a buscar lo que ha olvidado</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp33" id="resp33" value="2" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Ver si conoce a alguien por la zona que pueda prestarle dinero o tratar de que le vendan a crédito</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp33" id="resp33" value="3" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Dejar las compras para otra ocasión y aprovechar la oportunidad para dar un paseo por el barrio</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">34</span>
										M... está soltero. Ha tenido un año de trabajo muy fuerte y debe tomar una decisión sobre sus vacaciones ¿Qué elegirá?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp34" id="resp34" value="1" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Pasar un mes en la costa para poder leer, caminar y descansar</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp34" id="resp34" value="2" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Visitar una región del país que no conoce</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp34" id="resp34" value="3" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Hacer una larga marcha a pie o en bicicleta porque piensa que el deporte es el mejor de los descansos</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">35</span>
										¿Cuál es, a su modo de ver, la mejor manera de descansar durante el fin de semana para un hombre que trabaja todos los demás días?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp35" id="resp35" value="1" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Reunirse con los amigos</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp35" id="resp35" value="2" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Ir al cine</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp35" id="resp35" value="3" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Leer</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">36</span>
										Si usted dispusiera de un año de libertad y de los medios necesarios para hacer lo que quisiera ¿cuál eligiría entre las actividades siguientes?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp36" id="resp36" value="1" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Dar la vuelta al mundo en solitario a bordo de un velero</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp36" id="resp36" value="2" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Participar en una carrera automovilística Madrid- Bombay</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp36" id="resp36" value="3" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Visitar el mayor número posible de países</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">37</span>
										Un empleado administrativo, incorporado recientemente a la empresa, pide a X..., uno de sus compañeros, que le preste una pequeña suma de dinero hasta el mes siguiente ¿Qué hará X...?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp37" id="resp37" value="1" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Le prestará el dinero sin hacerle ninguna pregunta para no ponerle en un aprieto</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp37" id="resp37" value="2" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Le prestará el dinero porque a él le gustaría que le hicieran lo mismo si lo necesitara</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp37" id="resp37" value="3" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. No le dará el dinero, considerando que su compañero tiene, probablemente, buenos amigos dispuestos a ayudarle</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">38</span>
										B... se ha puesto, sin quererlo, fuera de la legalidad y debe escoger a un abogado para que le defienda. ¿Sobre cuál de los criterios siguientes basará su elección?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp38" id="resp38" value="1" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Su reputación de elocuencia</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp38" id="resp38" value="2" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Sus buenas relaciones con la magistratura</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp38" id="resp38" value="3" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Su perspicacia psicológica</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">39</span>
										E... ha sido nombrado recientemente para un puesto que implica responsabilidades jerárquicas importantes. ¿Qué actitud tomaría probablemente frente a sus subordinados?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp39" id="resp39" value="1" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Modificar su forma de mandar según las personas y las circunstancias</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp39" id="resp39" value="2" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Fijarse un sistema de mando bien elaborado e idéntico para todos</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp39" id="resp39" value="3" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Basándose en la experiencia, dejarse guiar por su intuición</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">40</span>
										R... tiene que hablar sobre informática a empleados que no tienen ningún conocimiento en este campo ¿Cuàl de las siguientes alternativas le planteará menos problemas?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp40" id="resp40" value="1" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Utilizar un vocabulario comprensible para todo el mundo</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp40" id="resp40" value="2" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Interesar al auditorio y mantener su atención</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp40" id="resp40" value="3" onclick="checkRespuesta(4);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Hacer participar a todos los asistentes</span>
										</label>
									</div>
								</div>
							</fieldset>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
									<a href="#" class="btn btn-success btn-block" id="btn_4" disabled>Siguiente Sección</a>
								</div>
							</div>
						</fieldset>
					</div>

					<div id="parte_5">
						<fieldset class="required" style="background: #ececec;">
							<h1>
								<div class="title-part text-center">
									SECCIÓN 5
								</div>
							</h1>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">41</span>
										L... asiste a un partido de fútbol entre dos equipos muy conocidos. El partido es muy disputado y los dos equipos rinden al máximo. Entre los espectadores el entusiasmo es tremendo:
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp41" id="resp41" value="1" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. L... se contenta con aplaudir las jugadas más brillantes</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp41" id="resp41" value="2" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. L... es dominado por la agitación general: grita y gesticula como los otros</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp41" id="resp41" value="3" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. L... discute con sus vecinos sobre las jugadas del futbolistas</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">42</span>
										B... atraviesa la calle por un paso de peatones con el semáforo en rojo. Un coche llega a gran velocidad y se detiene repentinamente a pocos centímetros de B ¿Cuál va a ser la reacción inmediata de éste?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp42" id="resp42" value="1" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Hará una observación irónica al mismo tiempo que maldice interiormente al conductor</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp42" id="resp42" value="2" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Dará airadamente su opinión al conductor sobre su manera de conducir</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp42" id="resp42" value="3" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Lanzará al conductor algunos insultros que no puede reprimir</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">43</span>
										A su modo de ver, ¿Cuál es, entre los siguientes aspectos, el màs desatendido actualmente en la educación de su hijo?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp43" id="resp43" value="1" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. La aceptación de responsabilidades</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp43" id="resp43" value="2" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. El gusto por el trabajo bien hecho</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp43" id="resp43" value="3" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. La capacidad para desenvolverse solo rápidamente</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">44</span>
										T... acaba de fracasar en un examen. Uno de sus amigos le dice que algunos fracasos son útiles en la vida. ¿Qué responderá él?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp44" id="resp44" value="1" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Se ve que no estás en mi lugar; yo preferiría no pasar por esto</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp44" id="resp44" value="2" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Puede que tengas razón</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp44" id="resp44" value="3" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Admite, por lo menos, que cuando ocurre es difícil de aceptar</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">45</span>
										L... se encuentra en una reunión amistosa. Sabe muy bien que la mayor parte de las personas presentes tienen opiniones muy diferentes a las suyas ¿Qué piensa usted que hará?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp45" id="resp45" value="1" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Evitar toda discusión</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp45" id="resp45" value="2" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Provocar la discusión</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp45" id="resp45" value="3" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Dar su opinión si no puede evitarlo</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">46</span>
										¿Qué cualidad le parece más útil en las relaciones sociales?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp46" id="resp46" value="1" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Tolerancia</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp46" id="resp46" value="2" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. La sinceridad</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp46" id="resp46" value="3" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. La aceptación de compromisos</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">47</span>
										M... acaba de modificar totalmente su apartamento. Invita a sus amigos a la inauguración. Entre los cumplidos que le hagan ¿Cuál será para él más agradable?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp47" id="resp47" value="1" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Una felicitación sobre su buen gusto</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp47" id="resp47" value="2" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Un elogio sobre la forma en que ha sacado partido de la disposición de las habitaciones</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp47" id="resp47" value="3" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Una alabanza a su originalidad, porque su decoración no se parece a ninguna otra ya vista</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">48</span>
										S... llega en el momento de una discusión entre dos personas a las que conoce poco. El tema del que hablan, muy específico, le es totalmente desconocido y apenas le interesa. ¿Cuál será su reacción?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp48" id="resp48" value="1" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Interviene para informarse, aún a riesgo de parecer ignorante</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp48" id="resp48" value="2" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Trata de interesarse porque no quiere parecer incorrecto</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp48" id="resp48" value="3" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. No interviene, porque no puede aportar un punto de vista interesante sobre el asunto</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">49</span>
										R... llega con retraso a una reunión de copropietarios ¿Qué es lo que probablemente le molestará más?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp49" id="resp49" value="1" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. La idea de que hayan podido aprovecharse de su ausencia para tomar una desición con la que él no esté de acuerdo</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp49" id="resp49" value="2" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. El temor de tener dificultad para integrarse en la discusión</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp49" id="resp49" value="3" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. La perspectiva de parecer descortés</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">50</span>
										Se ha pedido a F... un informe lo más objetivo posible sobre su modo de ser ¿Quién decidirá él que lo haga?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp50" id="resp50" value="1" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Su mujer</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp50" id="resp50" value="2" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Un grupo de amigos</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp50" id="resp50" value="3" onclick="checkRespuesta(5);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. El mismo</span>
										</label>
									</div>
								</div>
							</fieldset>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
									<a href="#" class="btn btn-success btn-block" id="btn_5" disabled>Siguiente Sección</a>		
								</div>
							</div>
						</fieldset>
					</div>

					<div id="parte_6">
						<fieldset class="required" style="background: #ececec;">
							<h1>
								<div class="title-part text-center">
									SECCIÓN 6
								</div>
							</h1>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">51</span>
										X ...  quiere construir una casa ¿Cuál de las siguientes soluciones elegirá?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp51" id="resp51" value="1" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Llamar a un arquitecto</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp51" id="resp51" value="2" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Hacer los planos él mismo y trabajar directamente con un constructor</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp51" id="resp51" value="3" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Hacer los planos con su amigo que acaba de hacer una casa</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">52</span>
										T... es un hombre de negocios generalmente muy ocupado. Una cita anulada a ùltima hora le permite tener una mañana libre totalmente imprevista ¿Cómo cree usted que la utilizará?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp52" id="resp52" value="1" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Aprovechará para levantarse temprano pronto y pasar la mañana practicando su deporte favorito</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp52" id="resp52" value="2" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Hará el balance de la semana que acaba de pasar</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp52" id="resp52" value="3" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Constituye para él una ocasión muy poco habitual de poder "perder su tiempo" y la aprovechará</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">53</span>
										D... va a ir a un país del que no conoce nada. Para preparar su viaje, ¿Qué cree usted que hará?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp53" id="resp53" value="1" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Leer libros y guías sobre ese país</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp53" id="resp53" value="2" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Preguntar a amigos o parientes que conocen bien el país</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp53" id="resp53" value="3" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Esperar a estar en el país para ver e informarse</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">54</span>
										Se oye frecuentemente decir que si una persona cae de repente sobrela acera a las 6 de la tarde, un momento en que las calles están abarrotadas, nadie ofrece su ayuda a la persona en dificultades ¿A qué cree usted que se debe esta conducta?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp54" id="resp54" value="1" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. A una total indiferencia por lo que puede ocurrir a los demás</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp54" id="resp54" value="2" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Al miedo ante una situación inesperada</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp54" id="resp54" value="3" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Al temor de ocuparse de lo que no le compete a uno</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">55</span>
										Una señora va a consultar a un medico sobre una infección que le parece bastante banal. El médico la interroga ampliamente antes de examinarla. Ella contesta, por supuesto, a las preguntas, pero, en el fondo, ¿qué pensará
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp55" id="resp55" value="1" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. En su lugar, yo comenzaría por hacer el examen y preguntaría después</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp55" id="resp55" value="2" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. El debe intentar conocer en qué circunstancias he cogido esta enfermedad</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp55" id="resp55" value="3" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Está bien que me haga preguntas, pero hay algunas en las que no veo relación ninguna con lo que yo tengo</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">56</span>
										C... tiene que contratar empleados de oficina; dispone de poco tiempo ¿Qué método empleará?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp56" id="resp56" value="1" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Tener una entrevista con los candidatos</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp56" id="resp56" value="2" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Examinar sus "curriculum vitae" y sus referencias</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp56" id="resp56" value="3" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Hacerles una prueba práctica en el trabajo</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">57</span>
										El mejor delegado de personal es aquel que:
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp57" id="resp57" value="1" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Es un buen negociador</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp57" id="resp57" value="2" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Está convencido de la justicia de la reivindicación</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp57" id="resp57" value="3" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Está bien integrado con el personal</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">58</span>
										La señora D... trata inútilmente de hacer que coma su hijo de cinco años; éste se resiste violentamente. ¿Qué hará ella?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp58" id="resp58" value="1" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Forzarle a comer considerando que el niño lo hace por capricho</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp58" id="resp58" value="2" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Hacerle un pequeño chantaje del tipo "Demuestra a mamá que eres bueno y que la quieres"</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp58" id="resp58" value="3" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Dejar al niño, pensando que comerá más en la comida siguiente</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">59</span>
										Se habla frecuentemente del respeto a las "reglas del juego". En general, se trata:
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp59" id="resp59" value="1" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. De una disposición profunda, estable y claramente definida</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp59" id="resp59" value="2" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. De la aceptación a desempeñar en cada instante el papel que conviene</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp59" id="resp59" value="3" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. De una sumisión sincera a las costumbres de un grupo</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">60</span>
										C..., que es aficionado al bricolage, decide tapizar él mismo un sillón valioso. Cuando llega a la mitad de su trabajo se da cuenta de que corre el peligro de fracasar en su intento ¿Cuál será su decisión?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp60" id="resp60" value="1" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Confiar el sillón a un especialista que realizará un trabajo impecable</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp60" id="resp60" value="2" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Deshacer lo que ha hecho y comenzar otra vez desde cero</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp60" id="resp60" value="3" onclick="checkRespuesta(6);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Dejar durante unos días su trabajo para emprenderlo cuando esté en mejores condiciones</span>
										</label>
									</div>
								</div>
							</fieldset>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
									<a href="#" class="btn btn-success btn-block" id="btn_6" disabled>Siguiente Sección</a>	
								</div>
							</div>
						</fieldset>
					</div>

					<div id="parte_7">
						<fieldset class="required" style="background: #ececec;">
							<h1>
								<div class="title-part text-center">
									SECCIÓN 7
								</div>
							</h1>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">61</span>
										C... va a ir con su mujer al campo durante la Pascua. Previendo la afluencia, ha reservado billetes de tren. Salen de su casa a la hora justa, pero un embotellamiento cerca de la estación le hace perder el tren ¿Cómo cree usted que reaccionará?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp61" id="resp61" value="1" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Siempre pasa lo mismo. SI hubieras estado preparada antes no hubiera ocurrido esto</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp61" id="resp61" value="2" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Se han fastidiado las vacaciones... No me apetece nada viajar de pie en el tren siguiente</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp61" id="resp61" value="3" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. ¡Qué le vamos a hacer! Voy a devolver los billetes y nos quedaremos en casa</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">62</span>
										A su juicio, la mayoría de los fracasos son debidos a:
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp62" id="resp62" value="1" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Incapacidad</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp62" id="resp62" value="2" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Coincidencia de circunstancias negativas</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp62" id="resp62" value="3" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Fallos ocasionales</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">63</span>
										F... ha encargado un frigorífico. En su ausencia, lo depositan en casa del concerje. El embalaje está estropeado, por lo que el frigorífico ha sufrido daños. F... decide hacer una gestión ante el distribuidor ¿De que forma se comportará?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp63" id="resp63" value="1" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Escribirá a los citados servicios conservando una copia de la carta</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp63" id="resp63" value="2" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Irá personalmente a reclamar</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp63" id="resp63" value="3" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Rogará a uno de sus amigos, empleado en esa casa, que intervenga</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">64</span>
										Con ocasión de un viaje organizado, B... se encuentra con personas que no conoce. Al cabo de unos días se da cuenta de que la mayoría de las personas le tienen poca simpatía ¿Cómo cree usted que reaccionará?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp64" id="resp64" value="1" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Le trae sin cuidado, porque para él lo importante es que el viaje esté bien organizado y que el guía sea eficaz</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp64" id="resp64" value="2" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Está un poco molesto y trata de hacer un esfuerzo para resultar más simpático</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp64" id="resp64" value="3" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Esto le amarga, en parte, sus vacaciones y propone que, en la próxima ocasión, se irá con sus amigos</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">65</span>
										R. hace un viaje con unos amigos. Una tarde el grupo se separa después de ponerse de acuerdo en la hora de salida. A lo largo de la tarde, R... descubre un monumento que le interesa mucho visitar, pero, si lo hace, no llegará a la cita ¿Qué hará?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp65" id="resp65" value="1" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Hace la visita porque piensa que no tendrá ya ocasión de volver a este lugar y que de todas maneras habrá otros que llegarán tarde también</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp65" id="resp65" value="2" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Renuncia a esta visita porque le parece más importante no hacer esperar a los demás</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp65" id="resp65" value="3" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Trata de encontrar a sus amigos para persuadirles de que hagan la visita con él, aún a riesgo de desorganizar la última parte del viaje</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">66</span>
										¿Cuál de estas profesiones le parece a usted la más envidiable?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp66" id="resp66" value="1" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Gerente de una pequeña empresa</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp66" id="resp66" value="2" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Director adjunto de una empres internacional</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp66" id="resp66" value="3" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Asesor científico de alto nivel</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">67</span>
										A causa de un accidente que ha causado la muerte de los padres de un niño pequeño, J... tiene que encargarse del niño con carácter definitivo. ¿Cuál será la actitud más probable que tendrá a este respecto?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp67" id="resp67" value="1" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Teme verse obligado a revisar su sistema de educación</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp67" id="resp67" value="2" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Confía en el buen carácter del niño</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp67" id="resp67" value="3" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Busca otras alternativas para el niño</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">68</span>
										Un amigo propone a T... llevarle en su velero a hacer un pequeño crucero. T... nunca ha practicado la vela ¿Qué cree usted que decidirá?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp68" id="resp68" value="1" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Aceptar, pensando que podrá desenvolverse bien</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp68" id="resp68" value="2" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Arreglárselas para hacer antes algunas horas de vela en una escuela</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp68" id="resp68" value="3" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. No aceptar, ante el temor de ser una moseltia para su amigo</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">69</span>
										T... va hacia el trabajo por su camino habitual. Una furgoneta de reparto le bloquea durante un buen rato en una calle estrecha. A juicio de usted, ¿Qué será lo más desagradable para T...?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp69" id="resp69" value="1" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. La perspectiva de llegar con retraso a su despacho</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp69" id="resp69" value="2" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. El hecho de tener que esperar sin poder moverse ni hacer nada</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp69" id="resp69" value="3" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Los toques de claxon - inútiles - de otros automovilistas</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">70</span>
										G. trabaja en una empresa para la que debe efectuar frecuentes desplazamientos. Acostumbra hacer 2 horas de atletismo todas las semanas en una sesión de entrenamiento colectivo. A juicio de usted, ¿cuál es su actitud en el curso de esta sesión?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp70" id="resp70" value="1" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Insiste en ciertas especialidades que le permitan mantenerse en buena forma</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp70" id="resp70" value="2" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Realiza todas las actividades propuestas, pero sin esforzarse demasiado, porque tiene un trabajo cansado</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp70" id="resp70" value="3" onclick="checkRespuesta(7);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Se entrega a fondo y frecuentemente temina agotado, porque no concibe el deporte de otra manera</span>
										</label>
									</div>
								</div>
							</fieldset>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
									<a href="#" class="btn btn-success btn-block" id="btn_7" disabled>Siguiente Sección</a>
								</div>
							</div>
						</fieldset>
					</div>

					<div id="parte_8">
						<fieldset class="required" style="background: #ececec;">
							<h1>
								<div class="title-part text-center">
									SECCIÓN 8
								</div>
							</h1>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">71</span>
										Según su criterio, ¿cuál es la ventaja de tener un perro en un apartamento de la ciudad ?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp71" id="resp71" value="1" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Permite sentirse menos solo</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp71" id="resp71" value="2" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Favorece la ocasión de caminar un poco cada día paseando al perro</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp71" id="resp71" value="3" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Vigila el apartamento</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">72</span>
										En el transcurso de una reunión en que S... se ha encontrado con amigos, les propone terminar la velada en su casa. Al enterarse, se une a ellos una persona que se mantenía alejada del grupo y a la que él estima poco ¿Cuál será la reacción de S...?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp72" id="resp72" value="1" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Piensa que, después de todo, tendrá ocasión de conocer mejor a esta persona</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp72" id="resp72" value="2" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Estimando que se ha invitado a sí misma, no se ocupará de ella en toda la noche</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp72" id="resp72" value="3" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Lamenta haber hablado demasiado alto</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">73</span>
										El director de un centro de delincuentes debe contratar a un nuevo educador ¿A cuál de estas cualidades dará más importancia?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp73" id="resp73" value="1" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Ser irreprochable y poder servir de modelo</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp73" id="resp73" value="2" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Tener buenos conocimientos pedagógicos</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp73" id="resp73" value="3" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Saber escuchar</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">74</span>
										Según su opinión ¿qué es más importante en una persona que da una fiesta?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp74" id="resp74" value="1" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Permitir a todos expresarse</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp74" id="resp74" value="2" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Invitar a personas que puedan coincidir con sus intereses</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp74" id="resp74" value="3" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Cuidar de que a nadie le falte nada</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">75</span>
										R... ha de exponer un proyecto a un colega que no conoce. Para que R... pueda hacerse compender por su interlocutor, es preferible que:
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp75" id="resp75" value="1" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Sepa cuál es su formación académica</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp75" id="resp75" value="2" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Conozca sus antecedentes profesionales</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp75" id="resp75" value="3" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Lea el informe sobre un trabajo hecho por su colega</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">76</span>
										Si tuviera que hacer algún reproche a la profesión médica ¿Cuál escogería?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp76" id="resp76" value="1" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. No consagra suficiente tiempo a cada enfermo</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp76" id="resp76" value="2" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Da demasiada importancia al dinero</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp76" id="resp76" value="3" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Utiliza a menudo un vocabulario incomprensible</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">77</span>
										N... tiene que escoger un plan de información para las personas de su departamento. Desearía una formación interesante para todos, pero, a la vez, eficaz y rentable ¿Qué eligirá?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp77" id="resp77" value="1" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Un plan organizado en función de las necesidades del departamento y en el que todas las fases estén definidas de antemano</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp77" id="resp77" value="2" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Un plan que tenga en cuenta a cada participante</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp77" id="resp77" value="3" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Un plan que incluya de manera regular exámenes de control</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">78</span>
										M... acaba de tener un ataque de hígado. Sin embargo un amigo le llama para invitarle a comer, precisando que le ha preparado su plato favorito. Supuesto que este plato está contraindicado para él en este momento ¿cuál será la reacción de M...?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp78" id="resp78" value="1" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Aceptar la invitación pensando que por una vez no hará caso de su régimen</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp78" id="resp78" value="2" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Proponer que se aplace esta invitación para más adelante, cuando acabe su régimen</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">79</span>
										M... no está satisfecho con la Seguridad Social. Lo que más le desagrada es:
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp79" id="resp79" value="1" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Lo absurdo de algunas disposiciones de esta entidad</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp79" id="resp79" value="2" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. La gran dificultad de atenerse a la complejidad de su reglamentación</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp79" id="resp79" value="3" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. La falta de atención a aspectos que él cree fundamentales</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">80</span>
										Y... ha decidido consagrar una parte de su tiempo libre a ayudar a las personas de la tercera edad. Existen varias posibilidades ¿cuál elegirá en último lugar cuando no hay otras alternativas?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp80" id="resp80" value="1" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Acompañar a las personas ancianas durante el paseo</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp80" id="resp80" value="2" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Visitar a una persona mayor cada semana</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp80" id="resp80" value="3" onclick="checkRespuesta(8);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Hacer colectas para la construcción de un hogar de ancianos</span>
										</label>
									</div>
								</div>
							</fieldset>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
									<a href="#" class="btn btn-success btn-block" id="btn_8" disabled>Siguiente Sección</a>
								</div>
							</div>
						</fieldset>
					</div>

					<div id="parte_9">
						<fieldset class="required" style="background: #ececec;">
							<h1>
								<div class="title-part text-center">
									SECCIÓN 9
								</div>
							</h1>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">81</span>
										Entre los siguientes argumentos publicitarios, ¿Cuál tiene más probabilidades de influir en el público?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp81" id="resp81" value="1" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. La muestra</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp81" id="resp81" value="2" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. El "slogan"</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp81" id="resp81" value="3" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. El testimonio de una "estrella"</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">82</span>
										En el transcurso de un viaje en tren, D... se encuentra en un compartimento abarrotado. Su vecino fuma y el humo le molesta mucho. Ya le ha rogado que dejara de fumar, sin resultado ¿Qué hará?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp82" id="resp82" value="1" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Pedir el parecer de otros viajeros del compartimento</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp82" id="resp82" value="2" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Abrir la ventana</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp82" id="resp82" value="3" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Hablar de nuevo con su vecino con el objeto de convencerle de que deje de fumar</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">83</span>
										El director del departamento de una empresa de productos alimenticios desea lanzar al mercado un nuevo postre. Dispone de un presupuesto que puede repartir en los 3 conceptos siguientes ¿En cuál de ellos empleará más dinero?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp83" id="resp83" value="1" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. En estudios de mercado para conocer los gustos de los futuros compradores</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp83" id="resp83" value="2" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. En publicidad para dar a conocer su producto</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp83" id="resp83" value="3" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. En la investigación de envases y presentación</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">84</span>
										B... toma parte en una discusión muy animada en el transcurso de la cual sus opiniones son muy atacadas. Entre las actitudes siguientes, ¿Cuál será la que adopte?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp84" id="resp84" value="1" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Tratar por todos los medios de convencer a los demás</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp84" id="resp84" value="2" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Divertirse simplemente en el juego de la discusión</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp84" id="resp84" value="3" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Dejarse convencer cuando vea que sus posiciones son poco defendibles</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">85</span>
										Cuando usted va a un restaurante, ¿qué tipo de plato prefiere?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp85" id="resp85" value="1" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Uno que ya conoce y que le gusta de modo especial</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp85" id="resp85" value="2" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. La especialidad de la casa</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp85" id="resp85" value="3" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Un plato exótico que usted desconoce en absoluto</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">86</span>
										Después de un naufrgio, C... llega sólo a una isla desierta. El clima es muy agradable y la vegetación le permitirá encontrar el alimento necesario. ¿Qué es, a su modo de ver, lo que probablemente le planteará menos problemas?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp86" id="resp86" value="1" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. El carácter nuevo e insólito de la situación</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp86" id="resp86" value="2" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. La soledad</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp86" id="resp86" value="3" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. La falta de algunos atractivos de la vida anterior</span>
										</label>
									</div>
								</div>
							</fieldset>

							<fieldset class="required">
								<h2>
									<div class="title-part">
										<span class="number">87</span>
										J... dirige una importante firma automovilista. Puede escoger entre varias políticas comerciales. ¿Cuál elegirá?
									</div>
								</h2>
								<div class="special-padding-row">
									<div class="label-cont">
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp87" id="resp87" value="1" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">A. Orientar sus esfuerzos para sacar de vez en cuando modelos especiales</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp87" id="resp87" value="2" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">B. Buscar los efectos de moda y atender a una clientela particular</span>
										</label>
										<label class="input-group input-group-radio row">
											<input type="radio" class="hidden-inputs" name="resp87" id="resp87" value="3" onclick="checkRespuesta(9);">
											<span class="input-group-addon"></span>
											<span class="input-group-title">C. Ofrecer de ordinario modelos clásicos</span>
										</label>
									</div>
								</div>
							</fieldset>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
									<input type="submit" value="Finalizar Prueba" class="btn btn-success btn-block" id="btn_9" disabled>
								</div>
							</div>
						</fieldset>
					</div>
				</form>
			</div>
		</section>
	</div>
@endsection

@section('js')
	<script src="{{ asset('js') }}/ipv.js"></script>
@endsection